//
//  Alarm.swift
//  AlarmClock
//
//  Created by Manpreet Singh Bedi on 21/07/15.
//  Copyright (c) 2015 Bazzinga Labs. All rights reserved.
//

import Foundation
import CoreData

class Alarm: NSManagedObject {

    @NSManaged var alarmTone: String
    @NSManaged var danceRoutine: String
    @NSManaged var enable: NSNumber
    @NSManaged var evilMode: NSNumber
    @NSManaged var label: String
    @NSManaged var limitSnooze: NSNumber
    @NSManaged var objectId: String
    @NSManaged var repeat: NSNumber
    @NSManaged var time: NSDate
    @NSManaged var stepCount: NSNumber

}
