//
//  AlarmDetailChangeDelegate.swift
//  AlarmClock
//
//  Created by Manpreet Singh Bedi on 30/06/15.
//  Copyright (c) 2015 Bazzinga Labs. All rights reserved.
//

import Foundation

protocol AlarmDetailChangeDelegate: class {
    func onAlarmDetailChanged(alarm: AlarmModel)
}