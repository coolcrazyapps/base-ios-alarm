//
//  AlarmDetailViewController.swift
//  AlarmClock
//
//  Created by Mohammad Shabaz Moosa on 28/06/15.
//  Copyright (c) 2015 Bazzinga Labs. All rights reserved.
//

import UIKit
import iAd

class AlarmDetailViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, SwitchSettingTableViewCellDelegate, SettingDetailChangeDelegate, SelectedOptionChangeDelegate, ADBannerViewDelegate {
    var alarm : AlarmModel = AlarmModel()  // new alarm
    var activeSetting: SettingModel?
    var delegate : AlarmDetailChangeDelegate?
    var rectangleAdView: ADBannerView?
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var timePicker: UIDatePicker!
    @IBOutlet weak var saveButton: UIBarButtonItem!
    @IBOutlet weak var cancelButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        if(!StoreHelper.isAdsRemoved())
        {
            initBannerAds()
        }
        self.tableView.tableFooterView = UIView()
        Themes.themes.initAlarmsDetailViewController(self)
    }
    
    override func viewWillAppear(animated: Bool) {
        // time picker needs to be set separately as its not part of tableview
        setTimePicker()
        self.title = isEditAlarm() ? "Edit Alarm": "Add Alarm"
    }
    
    func isEditAlarm() -> Bool {
        return alarm.objectId != ""
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setTimePicker()
    {
        timePicker.setDate(alarm.time, animated: true)
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        switch indexPath.section {
        case 0:
            switch indexPath.row {
            case 0:
                let cell = tableView.dequeueReusableCellWithIdentifier("SwitchSettingCell", forIndexPath: indexPath) as! SwitchSettingTableViewCell
                cell.setupCell(CellId.EvilMode, label: "Evil Mode", value: alarm.evilMode, delegate: self)
                return cell
            case 1:
                let cell = tableView.dequeueReusableCellWithIdentifier("LabelSettingCell", forIndexPath: indexPath) as! LabelSettingTableViewCell
                cell.setupCell(CellId.StepCount, label: "Number Of Steps", value: Utilities.valueToDisplayForOption(alarm.stepCount, options: Utilities.getStepCountOptions()))
                return cell
            case 2:
                let cell = tableView.dequeueReusableCellWithIdentifier("LabelSettingCell", forIndexPath: indexPath) as! LabelSettingTableViewCell
                cell.setupCell(CellId.SnoozeLimit, label: "Snooze Limit", value: Utilities.valueToDisplayForOption(alarm.limitSnooze, options: Utilities.getSnoozeOptions()))
                return cell
            case 3:
                let cell = tableView.dequeueReusableCellWithIdentifier("LabelSettingCell", forIndexPath: indexPath) as! LabelSettingTableViewCell
                cell.setupCell(CellId.AlarmTone, label: "Alarm Tone", value: Utilities.valueToDisplayForOption(alarm.alarmTone, options: Utilities.getAlarmToneOptions()))
                return cell
            case 4:
                let cell = tableView.dequeueReusableCellWithIdentifier("LabelSettingCell", forIndexPath: indexPath) as! LabelSettingTableViewCell
                cell.setupCell(CellId.Label, label: "Label", value: alarm.label)
                return cell
            default:
                return UITableViewCell()
            }
        case 1:
            let cell = tableView.dequeueReusableCellWithIdentifier("DeleteAlarmCell") as! DeleteAlarmTableViewCell
            cell.setupCell("Delete Alarm")
            // call Delete Function
            return cell
        default:
            return UITableViewCell()
        }
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0: return 5
        case 1: return 1
        default: return 1
        }
    }
    
    // MARK: Handle cell interaction
    func switchSettingOnValueChanged(cell: SwitchSettingTableViewCell, sender: UISwitch) {
        switch cell.id {
            case CellId.EvilMode:
                alarm.evilMode = cell.settingSwitch.on
            default: println("Unknown id \(cell.id)")
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        switch indexPath.section {
            case 1: confirmDeletion()
            case 0:
                switch indexPath.row {
                    case 1:
                    // Step count
                    activeSetting = SettingModel(id: CellId.StepCount, name: "Number Of Steps", value: String(alarm.stepCount), type: "number")
                    performSegueWithIdentifier("SelectOptionSegue", sender: self)
                    case 2:
                    // Snooze limit
                    activeSetting = SettingModel(id: CellId.SnoozeLimit, name: "Snooze Limit", value: String(alarm.limitSnooze), type: "number")
                    performSegueWithIdentifier("SelectOptionSegue", sender: self)
                    case 3:
                    // Alarm Tone
                        activeSetting = SettingModel(id: CellId.AlarmTone, name: "Alarm Tone", value: String(alarm.alarmTone), type: "string")
                        performSegueWithIdentifier("SelectOptionSegue", sender: self)
                    case 4:
                        // label setting
                        activeSetting = SettingModel(id: CellId.Label, name: "Label", value: alarm.label, type: "string")
                        performSegueWithIdentifier("TextInputSegue", sender: self)
                    
                    default: println("unknown")
                }
            default: println("Unknown selection")
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "TextInputSegue" {
            let destinationVC = segue.destinationViewController as! EditLabelViewController
            destinationVC.setting = activeSetting
            destinationVC.delegate = self
        }
        else if segue.identifier == "SelectOptionSegue" {
            var options : [Option] = []
            var selectedOption : Option = Option(id: "" ,name: "" ,value: "" ,type: "")
            if(activeSetting?.name == "Snooze Limit")
            {
                options = Utilities.getSnoozeOptions()
                selectedOption = Utilities.getSelectedOption(alarm.limitSnooze, options: options)
            }
            else if (activeSetting?.name == "Alarm Tone")
            {
                options = Utilities.getAlarmToneOptions()
                selectedOption = Utilities.getSelectedOption(alarm.alarmTone , options: options)
            }
            else if (activeSetting?.id == CellId.StepCount)
            {
                options = Utilities.getStepCountOptions()
                selectedOption = Utilities.getSelectedOption(alarm.stepCount , options: options)
            }
            let destinationVC = segue.destinationViewController as! SelectOptionTableViewController
            destinationVC.options = options
            destinationVC.title = activeSetting?.name
            destinationVC.selectedOption = selectedOption
            destinationVC.delegate = self
            
        }
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 1 {
            return 44
        }
        else {
            return 0
        }
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let  headerCell = tableView.dequeueReusableCellWithIdentifier("CustomHeaderCell") as! CustomHeaderTableViewCell
        switch section {
        case 1:
            headerCell.setupCell("Other Options")
            return headerCell
        default:
            headerCell.setupCell("Unknown section")
            return headerCell
        }
    }
    
    func updateAlarmDetails() {
        alarm.time = timePicker.date
    }
    
    @IBAction func saveAlarm(sender: UIBarButtonItem) {
        updateAlarmDetails()
        
        if(DatabaseController.databaseController.checkIfAlarmExists(alarm.objectId))
        {
            DatabaseController.databaseController.updateAlarmState(alarm)
        }
        else
        {
            alarm.objectId = NSUUID().UUIDString
            DatabaseController.databaseController.createAlarmRecord(alarm)
        }
        alarm.time = AlarmHandler.alarmHandler.setAlarm(alarm)
        delegate?.onAlarmDetailChanged(alarm)
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    @IBAction func cancelButtonDidTouch(sender: AnyObject) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func confirmDeletion(){
        var deleteAlert = UIAlertController(title: "Delete Alarm", message: "Are you sure you want to delete the alarm?", preferredStyle: UIAlertControllerStyle.Alert)
        
        deleteAlert.addAction(UIAlertAction(title: "Yes", style: .Default, handler: { (action: UIAlertAction!) in
            self.deleteAlarm()
        }))
        
        deleteAlert.addAction(UIAlertAction(title: "Cancel", style: .Default, handler: nil))
        
        presentViewController(deleteAlert, animated: true, completion: nil)
    }
    
    func deleteAlarm(){
        DatabaseController.databaseController.deleteAlarm(self.alarm.objectId)
        AlarmHandler.alarmHandler.deleteAlarm(self.alarm.objectId)
        delegate?.onAlarmDetailChanged(alarm)
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func onSettingDetailChanged(setting: SettingModel, sender: AnyObject) {
        switch setting.id {
            case CellId.Label: alarm.label = setting.value as! String
            default: println("Setting detail changed for \(setting.id)")
        }
        tableView.reloadData()
    }
    
    func onSelectedOptionChanged(option: Option) {
        if activeSetting?.id == CellId.SnoozeLimit {
            alarm.limitSnooze = option.value as! Int
        }
        if activeSetting?.id == CellId.AlarmTone {
            alarm.alarmTone = option.value as! String
        }
        if activeSetting?.id == CellId.StepCount {
            alarm.stepCount = option.value as! Int
        }
        
        tableView.reloadData()
    }
    
    /* Display Banner Ads : Init */
    func initBannerAds()
    {
        self.canDisplayBannerAds = true
        rectangleAdView = ADBannerView(adType: ADAdType.Banner)
        rectangleAdView?.delegate = self
    }
    
    func bannerViewDidLoadAd(banner: ADBannerView!) {
        self.view.addSubview(banner)
        self.view.layoutIfNeeded()
    }
    
    func bannerView(banner: ADBannerView!, didFailToReceiveAdWithError error: NSError!) {
        banner.removeFromSuperview()
        self.view.layoutIfNeeded()
    }
    /* Display Banner Ads : End */

}
