//
//  CreateAlarm.swift
//  AlarmClock
//
//  Created by Manpreet Singh Bedi on 01/07/15.
//  Copyright (c) 2015 Bazzinga Labs. All rights reserved.
//

import Foundation
import UIKit

class AlarmHandler
{
    static let alarmHandler = AlarmHandler()
    
    func setAlarm( alarm : AlarmModel) -> NSDate
    {
        return setAlarm(alarm, time: alarm.time)
    }

    func setAlarm( alarm : AlarmModel, time: NSDate) -> NSDate
    {
        return setAlarm(alarm, time: time, snoozeCount: alarm.limitSnooze)
    }
    
    func setAlarm(alarm : AlarmModel, time: NSDate, snoozeCount: Int) -> NSDate {
        // as a precaution, remove earlier instances of alarm
        cancelLocalNotificationWithId(alarm.objectId)
        
        var alarmTime = pushTimeToFutureIfNeeded(time)
        let alarmTimeDescription = alarmTime.descriptionWithLocale(NSLocale.systemLocale())
        
        var notification = UILocalNotification()
        notification.alertBody = alarm.hasValidLabel() ? alarm.label : "Wake up" // text that will be displayed in the notification
        notification.alertTitle = NSBundle.mainBundle().infoDictionary!["CFBundleName"] as! String

        notification.fireDate = alarmTime // todo item due date (when notification will be fired)
        notification.soundName = alarm.alarmTone // play default sound
        notification.userInfo = ["snoozeCount" : snoozeCount , "notificationID" : alarm.objectId]
        notification.repeatInterval = NSCalendarUnit.CalendarUnitMinute
        UIApplication.sharedApplication().scheduleLocalNotification(notification)
        
        // add one more notification to trigger every 30 seconds
        var secondNotificationTime = alarmTime.dateByAddingTimeInterval(30)
        notification.fireDate = secondNotificationTime
        notification.alertBody = "Wake up"
        UIApplication.sharedApplication().scheduleLocalNotification(notification)
        
        let alarmTriggerTime = alarmTime.descriptionWithLocale(NSLocale.systemLocale())
        println("Set alarm for \(alarmTriggerTime)")
        
        return alarmTime
    }
    
    func pushTimeToFutureIfNeeded(time: NSDate) -> NSDate {
        var now = NSDate()
        
        // if in past
        if Int(time.timeIntervalSinceNow) < 0 {
            let cal = NSCalendar.currentCalendar()
            println("Alarm time in past. Pushing forward")
            
            // get time components
            var sourceTimeComponents = cal.components(.CalendarUnitDay | .CalendarUnitMonth | .CalendarUnitYear | .CalendarUnitMinute | .CalendarUnitSecond | .CalendarUnitHour , fromDate: time)
            var nowTimeComponents = cal.components(.CalendarUnitDay | .CalendarUnitMonth | .CalendarUnitYear | .CalendarUnitMinute | .CalendarUnitSecond | .CalendarUnitHour , fromDate: now)
            
            nowTimeComponents.second = 0
            nowTimeComponents.minute = sourceTimeComponents.minute
            nowTimeComponents.hour = sourceTimeComponents.hour
            
            var targetDate = cal.dateFromComponents(nowTimeComponents)!
            
            // still in past
            if now.compare(targetDate) == NSComparisonResult.OrderedDescending {
                // add a day
                return targetDate.dateByAddingTimeInterval(86400)
            }
            else {
                return targetDate
            }
        }
        else if Int(time.timeIntervalSinceNow) > 86400 {
            println("Alarm time in future. Pulling backward by a day")
            return time.dateByAddingTimeInterval(-86400)
        }
        else {
            return time
        }
    }
    
    func disableAlarm(objectId : String)
    {
        cancelLocalNotificationWithId(objectId)
    }
    func enableAlarm(objectId : String) -> NSDate
    {
        return setAlarm(DatabaseController.databaseController.getAlarmData(objectId))
    }
    func deleteAlarm(objectId : String)
    {
        cancelLocalNotificationWithId(objectId)
    }
    func openAlarmScreen(window : UIWindow?, alarm: AlarmModel, snoozeCount: Int)
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let alarmRingingViewController = storyboard.instantiateViewControllerWithIdentifier("AlarmRingingScreen") as! AlarmRingingViewController
        alarmRingingViewController.alarm = alarm
        alarmRingingViewController.snoozeCount = snoozeCount
        window?.rootViewController = alarmRingingViewController
        window?.makeKeyAndVisible()
    }
    func cancelLocalNotificationWithId(notificationId : String)
    {
        println("Canceling notification with id: \(notificationId)")
        var matchingNotificationCount = 0, totalNotificationCount = 0
        for notification in UIApplication.sharedApplication().scheduledLocalNotifications {
            totalNotificationCount++
            if let id = (notification as! UILocalNotification).userInfo?["notificationID"] as? String {
                if(id == notificationId)
                {
                    UIApplication.sharedApplication().cancelLocalNotification(notification as! UILocalNotification)
                    matchingNotificationCount++
                }
            }
        }
        println("Found \(totalNotificationCount) notifications. Canceled \(matchingNotificationCount)")
    }
}
