//
//  AlarmModel.swift
//  AlarmClock
//
//  Created by Manpreet Singh Bedi on 28/06/15.
//  Copyright (c) 2015 Bazzinga Labs. All rights reserved.
//

import Foundation

class AlarmModel{
    
    var danceRoutine: String = ""
    var limitSnooze: Int = 0
    var evilMode: Bool = false
    var repeat: Bool = false
    var alarmTone: String = Constants.DEFAULT_ALARM_TONE
    var label: String = ""
    var time: NSDate = NSDate()
    var objectId: String = ""
    var enable: Bool = true
    var stepCount: Int = 0
    
    init() {
        self.time = getClosestValidTime()
        loadDefaults()
    }
    
    func loadDefaults() {
        if(SharedPreferences.preferences.checkKey(Constants.ALARM_EVIL_MODE))
        {
            self.evilMode = SharedPreferences.preferences.getKeyValue(Constants.ALARM_EVIL_MODE).toBool()
        }
        if(SharedPreferences.preferences.checkKey(Constants.ALARM_SNOOZE_LIMIT))
        {
            self.limitSnooze = SharedPreferences.preferences.getValueOrDefault(Constants.ALARM_SNOOZE_LIMIT, defaultValue: Constants.DEFAULT_SNOOZE_LIMIT) as! Int
        }
        if(SharedPreferences.preferences.checkKey(Constants.ALARM_TONE))
        {
            self.alarmTone = SharedPreferences.preferences.getKeyValue(Constants.ALARM_TONE)
        }
        
        self.stepCount = SharedPreferences.preferences.getValueOrDefault(Constants.ALARM_STEP_COUNT, defaultValue: Constants.DEFAULT_STEP_COUNT) as! Int
        
    }
    
    func getClosestValidTime() -> NSDate{
        let now = NSDate()
        let cal = NSCalendar.currentCalendar()
        var components = cal.components(.CalendarUnitDay | .CalendarUnitMonth | .CalendarUnitYear | .CalendarUnitMinute | .CalendarUnitSecond | .CalendarUnitHour , fromDate: now)
        components.second = 0 // zero out the seconds
        
        var dateWithZeroSeconds = cal.dateFromComponents(components)!
        if components.minute % Constants.VALID_ALARM_TIME_INTERVAL_IN_MINUTES == 0 {
            return dateWithZeroSeconds
        }
        else {
            var minutesToAdd = 5 - components.minute % 5;
            let offsetDate = cal.dateByAddingUnit(.CalendarUnitMinute, value: minutesToAdd, toDate: dateWithZeroSeconds, options: NSCalendarOptions.MatchFirst)!
            return offsetDate
        }
        
    }
    
    func getFormattedDate() -> String {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "hh:mm a"
        return dateFormatter.stringFromDate(time)
    }
    
    // Cleans up alarm data to store valid data
    func normalizeFields() {
        if isAlarmSetInPast() {
            // advance date to next possible time
            
        }
    }
    
    func isAlarmSetInPast() -> Bool {
        let now = NSDate()
        return now.compare(self.time) == NSComparisonResult.OrderedDescending
    }
    
    func hasValidLabel() -> Bool {
        return label.length > 0
    }
}
