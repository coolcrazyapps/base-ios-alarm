//
//  AlarmRingingViewController.swift
//  AlarmClock
//
//  Created by Mohammad Shabaz Moosa on 08/07/15.
//  Copyright (c) 2015 Bazzinga Labs. All rights reserved.
//

import UIKit
import AVFoundation

class AlarmRingingViewController: UIViewController, StepDetectionDelegate {
    
    var alarm: AlarmModel!
    var snoozeCount: Int!
    var audioPlayer = AVAudioPlayer()
    var isDismissed = false
    var stepsTaken = 0
    var stepHelper: StepHelper!
    
    @IBOutlet weak var alarmLabel: UILabel!
    @IBOutlet weak var stepsProgressView: CircleProgressView!
    @IBOutlet weak var stepsLabel: UILabel!
    
    override func viewWillAppear(animated: Bool) {
        // setup ui
        setupUI()
        stepHelper = StepHelper()
        stepHelper.delegate = self
    }
    
    func onStep() {
        stepsTaken++
        updateStepView()
    }
    
    func onShake() {
        stepsTaken -= alarm.stepCount/2
        stepsTaken = stepsTaken < 0 ? 0: stepsTaken
        updateStepView()
    }
    
    func setupUI() {
        if alarm.hasValidLabel() {
            alarmLabel.text = alarm.label
        }
        else {
            alarmLabel.hidden = true
        }
        
        updateStepView()
    }
    
    func updateStepView(){
        var remainingSteps = getRemainingSteps()
        
        stepsProgressView.progress = Double(stepsTaken)/Double(alarm.stepCount)
        stepsLabel.text = String(remainingSteps)
        
        if remainingSteps == 0 && !isDismissed {
            exitAlarm()
        }
    }
    
    func getRemainingSteps() -> Int {
        var remainingSteps = alarm.stepCount - stepsTaken
        
        return remainingSteps < 0 ? 0: remainingSteps // normalize step display screwed up
    }
    
    @IBAction func onSnoozeDidTouch(sender: DesignableButton) {
        if alarm.evilMode {
            JLToast.makeText("Evil mode enabled. You can't snooze").show()
        }
        else if snoozeCount == 0 {
            JLToast.makeText("You've hit the snooze limit. No more snoozes for you").show()
        }
        else {
            snoozeNotifications()
            
            exitAlarm()
        }
    }
    
    func snoozeNotifications(){
        snoozeCount = snoozeCount - 1
        var snoozeDuration = NSTimeInterval(60 * (SharedPreferences.preferences.getValueOrDefault(Constants.ALARM_SNOOZE_DURATION, defaultValue: Constants.DEFAULT_SNOOZE_DURATION) as! Int))
        let triggerTime = NSDate(timeIntervalSinceNow: snoozeDuration)
        AlarmHandler.alarmHandler.setAlarm(alarm, time: triggerTime, snoozeCount: snoozeCount)
        let snoozeDurationInMinutes = Int(snoozeDuration/60 as Double)
        JLToast.makeText("Alarm snoozed for \(snoozeDurationInMinutes) minutes").show()
    }
    
    func exitAlarm() {
        isDismissed = true
        if audioPlayer.playing {
            audioPlayer.stop()
        }
        performSegueWithIdentifier("ExitToHomeScreen", sender: self)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        playAlarmSound()
    }
    
    func playAlarmSound() {
        var alarmToneTemp = alarm.alarmTone
        var alarmTone = alarmToneTemp.substringWithRange(Range<String.Index>(start: alarmToneTemp.startIndex, end: advance(alarmToneTemp.endIndex, -4)))
        println(alarmTone)
        var alertSound = NSURL(fileURLWithPath: NSBundle.mainBundle().pathForResource(alarmTone, ofType: "mp3")!)
        var error:NSError?
        AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback, error: nil)
        AVAudioSession.sharedInstance().setActive(true, error: nil)
        audioPlayer = AVAudioPlayer(contentsOfURL: alertSound, error: &error)
        audioPlayer.numberOfLoops = -1
        audioPlayer.prepareToPlay()
        audioPlayer.play()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
