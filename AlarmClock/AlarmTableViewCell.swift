//
//  AlarmTableViewCell.swift
//  AlarmClock
//
//  Created by Mohammad Shabaz Moosa on 27/06/15.
//  Copyright (c) 2015 Bazzinga Labs. All rights reserved.
//

import UIKit

class AlarmTableViewCell: UITableViewCell {

    @IBOutlet weak var roundedView: DesignableView!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var enabledSwitch: UISwitch!
    @IBOutlet weak var cellBorder: UIView!
    var delegate: AlarmDetailChangeDelegate?
    var alarm : AlarmModel = AlarmModel()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let width : CGFloat = 350
        self.frame.size.width = width
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupCell() {
        timeLabel.text = alarm.getFormattedDate()
        descriptionLabel.text = alarm.label
        enabledSwitch.setOn(alarm.enable, animated: false)
        Themes.themes.initAlarmTableViewCell(self as AlarmTableViewCell , alarmModel: alarm)
    }
    
    @IBAction func switchIsChanged(mySwitch: UISwitch) {
        if mySwitch.on {
            alarm.enable = true
            alarm.time = AlarmHandler.alarmHandler.enableAlarm(self.alarm.objectId)
            
        } else {
            alarm.enable = false
            AlarmHandler.alarmHandler.disableAlarm(self.alarm.objectId)
        }
        if(DatabaseController.databaseController.checkIfAlarmExists(alarm.objectId))
        {
            DatabaseController.databaseController.updateAlarmState(alarm)
            delegate?.onAlarmDetailChanged(alarm)
        }
    }
}
