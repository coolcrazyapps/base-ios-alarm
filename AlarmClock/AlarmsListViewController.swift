//
//  AlarmsListViewController.swift
//  AlarmClock
//
//  Created by Mohammad Shabaz Moosa on 27/06/15.
//  Copyright (c) 2015 Bazzinga Labs. All rights reserved.
//

import UIKit
import iAd


class AlarmsListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, AlarmDetailChangeDelegate, ADBannerViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var centeredAddAlarmButton: DesignableButton!
    @IBOutlet weak var addButton: UIBarButtonItem!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var addAlarmLabel: UILabel!
    var rectangleAdView: ADBannerView?

    var alarms = [AlarmModel]() //TODO: change to real code
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        if(!StoreHelper.isAdsRemoved())
        {
            initBannerAds()
        }
        Themes.themes.initAlarmsListViewController(self)
        setupDateDetails()
    }
    
    override func viewDidAppear(animated: Bool) {
        setupDateDetails()
    }
    
    override func viewWillAppear(animated: Bool) {
        setupAlarmDetails()
    }
    
    func setupLayout(){
        if alarms.count > 0 {
            tableView.hidden = false
            centeredAddAlarmButton.hidden = true
            dateLabel.hidden = false
            dayLabel.hidden = false
            addAlarmLabel.hidden = true
        }
        else {
            dateLabel.hidden = true
            dayLabel.hidden = true
            tableView.hidden = true
            centeredAddAlarmButton.hidden = false
            addAlarmLabel.hidden = false
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let alarm = alarms[indexPath.row]
        let cell = tableView.dequeueReusableCellWithIdentifier("AlarmCell") as! AlarmTableViewCell
        cell.alarm = alarm
        cell.delegate = self
        cell.setupCell()
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        var cell = tableView.cellForRowAtIndexPath(indexPath)
        performSegueWithIdentifier("AlarmDetailSegue", sender: cell)
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return alarms.count
    }
    
    @IBAction func addNewAlarmOnTouch(sender: AnyObject) {
        performSegueWithIdentifier("AlarmDetailSegue", sender: sender)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "AlarmDetailSegue" {
            // if source is an alarm selected
            if let indexPath = tableView.indexPathForSelectedRow() {
                // attach alarm data
                let destinationNavigationController = segue.destinationViewController as! UINavigationController
                let destinationViewController = destinationNavigationController.viewControllers[0] as! AlarmDetailViewController
                destinationViewController.delegate = self
                destinationViewController.alarm = alarms[indexPath.row]
            }
            else
            {
                let destinationNavigationController = segue.destinationViewController as! UINavigationController
                let destinationViewController = destinationNavigationController.viewControllers[0] as! AlarmDetailViewController
                destinationViewController.delegate = self
            }
        }
    }
    
    func onAlarmDetailChanged(alarm: AlarmModel)
    {
        // if not delete alarm scenario
        if DBAlarmHandler.dbAlarmHandler.checkIfAlarmExists(alarm.objectId) {
            if alarm.enable {
                JLToast.makeText("Alarm set \(Utilities.getTimeFromNow(alarm.time)) from now").show()
            }
        }
        setupAlarmDetails()
    }
    
    func setupAlarmDetails()
    {
        alarms = DBAlarmHandler.dbAlarmHandler.getAllData()
        tableView.reloadData()
        setupLayout()
        setupDateDetails()
    }
    
    func setupDateDetails()
    {
        var todaysDate:NSDate = NSDate()
        var dateFormatter:NSDateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        var today:String = dateFormatter.stringFromDate(todaysDate)
        dateLabel.text = today
        dayLabel.text = Utilities.getWeekDay(today)
    }
    /* Display Banner Ads : Init */
    func initBannerAds()
    {
        self.canDisplayBannerAds = true
        rectangleAdView = ADBannerView(adType: ADAdType.Banner)
        rectangleAdView?.delegate = self
    }
    
    func bannerViewDidLoadAd(banner: ADBannerView!) {
        self.view.addSubview(banner)
        self.view.layoutIfNeeded()
    }
    
    func bannerView(banner: ADBannerView!, didFailToReceiveAdWithError error: NSError!) {
        banner.removeFromSuperview()
        self.view.layoutIfNeeded()
    }
    /* Display Banner Ads : End */
}
