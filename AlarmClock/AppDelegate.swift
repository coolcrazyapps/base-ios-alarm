//
//  AppDelegate.swift
//  AlarmClock
//
//  Created by Mohammad Shabaz Moosa on 27/06/15.
//  Copyright (c) 2015 Bazzinga Labs. All rights reserved.
//

import UIKit
import CoreData
import Batch

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, BatchUnlockDelegate {
    
    var window: UIWindow?
    static let DBManagedObjectContext = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        
        UIApplication.sharedApplication().idleTimerDisabled = true
        UIApplication.sharedApplication().statusBarHidden = false
        UIApplication.sharedApplication().statusBarStyle = UIStatusBarStyle.LightContent
        let notification = launchOptions?[UIApplicationLaunchOptionsLocalNotificationKey] as! UILocalNotification!
        if (notification != nil) {
            println("BOND")
            //AlarmHandler.alarmHandler.openAlarmScreen(window, alarm)
        }
        initAlarmClock(launchOptions)
        application.registerUserNotificationSettings(UIUserNotificationSettings(forTypes: .Alert | .Badge | .Sound, categories: nil))
       
        return true
    }
    
    func applicationWillResignActive(application: UIApplication) {
        // Trigger notifications for alarm if home button is pressed
        triggerActiveAlarmNotifications()
    }
    
    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        triggerActiveAlarmNotifications()
        self.saveContext()
    }
//    func application(application: UIApplication, handleActionWithIdentifier identifier: String?, forLocalNotification notification: UILocalNotification, completionHandler: () -> Void) {
//        // Must be called when finished
//        AlarmHandler.alarmHandler.alarmActionHandler(identifier!, notification: notification , window: window)
//        completionHandler()
//    }
    
    func triggerActiveAlarmNotifications() {
        println("checking for active alarms")
        if self.window?.rootViewController is AlarmRingingViewController {
            let alarmRingingViewController = self.window?.rootViewController as! AlarmRingingViewController
            if !alarmRingingViewController.isDismissed {
                println("found active alarm")
                let currentAlarm = alarmRingingViewController.alarm
                let alarmTime = NSDate().dateByAddingTimeInterval(1)
                AlarmHandler.alarmHandler.setAlarm(currentAlarm, time: alarmTime)
            }
        }
    }
    
    /* ALARM CLOCK INIT START */
    
    func initAlarmClock(launchOptions: [NSObject: AnyObject]?)
    {
        initBatch(self)
        getNotifications()
        ParseApplication.parseApplication.initParse(didFinishLaunchingWithOptions: launchOptions)
        if(!SharedPreferences.preferences.checkKey(Constants.IS_FIRST_TIME))
        {
            SharedPreferences.preferences.initSharedPreferences()
            SharedPreferences.preferences.putKey(Constants.IS_FIRST_TIME, value: "DONE")
        }
        Themes.themes.initThemes(window)
    }
    func getNotifications()
    {
        for notification in UIApplication.sharedApplication().scheduledLocalNotifications {
            if let id = (notification as! UILocalNotification).userInfo?["notificationID"] as? String {
                if(!DatabaseController.databaseController.checkIfAlarmExists(id)) {
                    AlarmHandler.alarmHandler.cancelLocalNotificationWithId(id)
                }
            }
        }
    }
    
    /* ALARM CLOCK INIT END */
    
    // MARK: - Core Data stack
    
    lazy var applicationDocumentsDirectory: NSURL = {
        // The directory the application uses to store the Core Data store file. This code uses a directory named "com.bazzingalabs.AlarmClock" in the application's documents Application Support directory.
        let urls = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)
        return urls[urls.count-1] as! NSURL
        }()
    
    lazy var managedObjectModel: NSManagedObjectModel = {
        // The managed object model for the application. This property is not optional. It is a fatal error for the application not to be able to find and load its model.
        let modelURL = NSBundle.mainBundle().URLForResource("AlarmClock", withExtension: "momd")!
        return NSManagedObjectModel(contentsOfURL: modelURL)!
        }()
    
    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator? = {
        // The persistent store coordinator for the application. This implementation creates and return a coordinator, having added the store for the application to it. This property is optional since there are legitimate error conditions that could cause the creation of the store to fail.
        // Create the coordinator and store
        var coordinator: NSPersistentStoreCoordinator? = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.URLByAppendingPathComponent("AlarmClock.sqlite")
        var error: NSError? = nil
        var failureReason = "There was an error creating or loading the application's saved data."
        if coordinator!.addPersistentStoreWithType(NSSQLiteStoreType, configuration: nil, URL: url, options: nil, error: &error) == nil {
            coordinator = nil
            // Report any error we got.
            var dict = [String: AnyObject]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data"
            dict[NSLocalizedFailureReasonErrorKey] = failureReason
            dict[NSUnderlyingErrorKey] = error
            error = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
            // Replace this with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog("Unresolved error \(error), \(error!.userInfo)")
            abort()
        }
        
        return coordinator
        }()
    
    lazy var managedObjectContext: NSManagedObjectContext? = {
        // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to fail.
        let coordinator = self.persistentStoreCoordinator
        if coordinator == nil {
            return nil
        }
        var managedObjectContext = NSManagedObjectContext()
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
        }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        if let moc = self.managedObjectContext {
            var error: NSError? = nil
            if moc.hasChanges && !moc.save(&error) {
                // Replace this implementation with code to handle the error appropriately.
                // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                NSLog("Unresolved error \(error), \(error!.userInfo)")
                abort()
            }
        }
    }
    
    /* Batch Start */
    
    func initBatch(batchUnlockDelegate : BatchUnlockDelegate)
    {
        BatchUnlock.setupUnlockWithDelegate(batchUnlockDelegate)
        Batch.startWithAPIKey(Constants.BATCH_API_KEY)
    }
    
    func automaticOfferRedeemed(offer: BatchOffer!)
    {
        // Give the given quantity of the resource to the user
        let additionalParameters = offer.offerAdditionalParameters()
        if(additionalParameters.indexForKey("snooze") != nil)
        {
            let snooze = additionalParameters["snooze"] as? String
            println(snooze)
        }
        if(additionalParameters.indexForKey("ads") != nil)
        {
            let ads = additionalParameters["ads"] as? String
            println(ads)
        }
        if(additionalParameters.indexForKey("ringtones") != nil)
        {
            let ringtones = additionalParameters["ringtones"] as? String
            println(ringtones)
        }
        if(additionalParameters.indexForKey("themes") != nil)
        {
            let ringtones = additionalParameters["themes"] as? String
            println(ringtones)
        }
    }
    
    /* Batch End */
    
    func application(application: UIApplication, didReceiveLocalNotification notification: UILocalNotification) {
        println("Notification received")
        
        let state = application.applicationState
        if let alarmId = notification.userInfo?["notificationID"] as? String {
            if DatabaseController.databaseController.checkIfAlarmExists(alarmId) {
                // get alarm details
                let alarm = DatabaseController.databaseController.getAlarmData(alarmId)
                
                // cancel further notifications
                AlarmHandler.alarmHandler.cancelLocalNotificationWithId(alarmId)
                
                // get snooze count
                let snoozeCount = notification.userInfo?["snoozeCount"] as! Int
                
                // open alarm ringing screen
                AlarmHandler.alarmHandler.openAlarmScreen(window, alarm: alarm, snoozeCount: snoozeCount)
            }
            else {
                println("ERROR: Unknown notification \(notification.userInfo)")
            }
        }
    }
    
    
}

