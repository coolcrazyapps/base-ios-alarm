//
//  CellId.swift
//  AlarmClock
//
//  Created by Mohammad Shabaz Moosa on 29/06/15.
//  Copyright (c) 2015 Bazzinga Labs. All rights reserved.
//

import UIKit

class CellId {
    static var EvilMode: String = "evil_mode";
    static var SnoozeDuration = "snooze_duration";
    static var SnoozeLimit = "snooze_limit";
    static var AlarmTone = "alarm_tone";
    static var Vibrate = "vibrate";
    static var Label = "label";
    static var StepCount = "step_count";
    static var StepSensitivity = "step_sensitivity";
    static var ShakeSensitivity = "shake_sensitivity";
}
