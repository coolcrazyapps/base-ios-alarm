//
//  Constants.swift
//  AlarmClock
//
//  Created by Manpreet Singh Bedi on 29/06/15.
//  Copyright (c) 2015 Bazzinga Labs. All rights reserved.
//

import Foundation

class Constants {
    static let VALID_ALARM_TIME_INTERVAL_IN_MINUTES = 5
    static let TIME_INTERVAL_SINCE_NOW : NSTimeInterval = 60
    static let SNOOZE_INTERVAL_IN_MINUTES = 5
    static let PURCHASE_LIMIT_SNOOZE = "LIMIT SNOOZE"
    static let PURCHASE_NO_ADS = "NO ADS"
    static let PURCHASE_RINGTONES = "RINGTONES"
    static let PURCHASE_THEMES = "THEMES"
    static let BATCH_API_KEY : String = "DEV559963D7A56DA0C79061E860D47"
    
    static let DEFAULT_ALARM_TONE : String = "Alarm.mp3"
    static let DEFAULT_SNOOZE_DURATION : Int = 5
    static let DEFAULT_SNOOZE_LIMIT : Int = -1
    static let DEFAULT_STEP_COUNT : Int = 15
    static let DEFAULT_STEP_SENSITIVITY : Float = 0.25
    static let DEFAULT_SHAKE_SENSITIVITY : Float = 0.6
    
    static let ALARM_EVIL_MODE : String = "EVIL_MODE"
    static let ALARM_SNOOZE_DURATION : String = "SNOOZE_DURATION"
    static let ALARM_SNOOZE_LIMIT : String = "SNOOZE_LIMIT"
    static let ALARM_TONE : String = "ALARM_TONE"
    static let ALARM_STEP_COUNT : String = "ALARM_STEP_COUNT"
    
    static let STEP_SENSITIVITY: String = "STEP_SENSITIVITY"
    static let SHAKE_SENSITIVITY: String = "SHAKE_SENSITIVITY"
    static let MAIL_RECIPIENT = ["walkmeup@gmail.com"]
    static let APP_URL = "http://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?id=1015584705&pageNumber=0&sortOrdering=1&mt=8"
    static let IS_FIRST_TIME = "IS_FIRST_TIME"
}
