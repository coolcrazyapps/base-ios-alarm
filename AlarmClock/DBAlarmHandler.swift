//
//  DBProfileHandler.swift
//  wordify
//
//  Created by Manpreet Singh Bedi on 31/05/15.
//  Copyright (c) 2015 Girish  Budhwani. All rights reserved.
//

import Foundation
import CoreData

class DBAlarmHandler
{
    static let dbAlarmHandler = DBAlarmHandler()
    static let entityDescription = NSEntityDescription.entityForName("Alarm", inManagedObjectContext: AppDelegate.DBManagedObjectContext!)
    static let entityName = NSStringFromClass(Alarm.classForCoder())
    
    func createAlarmRecord(alarm: AlarmModel)
    {
        let alarmRecord  = Alarm(entity: DBAlarmHandler.entityDescription!,insertIntoManagedObjectContext: AppDelegate.DBManagedObjectContext)
        alarmRecord.objectId = alarm.objectId
        alarmRecord.time = alarm.time
        alarmRecord.danceRoutine = alarm.danceRoutine
        alarmRecord.evilMode = alarm.evilMode
        alarmRecord.limitSnooze = alarm.limitSnooze
        alarmRecord.alarmTone = alarm.alarmTone
        alarmRecord.repeat = alarm.repeat
        alarmRecord.label = alarm.label
        alarmRecord.enable = alarm.enable
        alarmRecord.stepCount = alarm.stepCount
        //Save Level Record
        
        var error: NSError?
        AppDelegate.DBManagedObjectContext?.save(&error)
        if let err = error {
            println(err.description)
        }
        else
        {
            println("Success")
        }
    }
    
    func getAllData() -> [AlarmModel]
    {
        var alarmAll = [AlarmModel]()
        let request = NSFetchRequest()
        request.entity = DBAlarmHandler.entityDescription
        var error: NSError?
        var objects = AppDelegate.DBManagedObjectContext?.executeFetchRequest(request, error: &error)
        if let alarmDetails = objects {
            if alarmDetails.count > 0 {
                for alarm in alarmDetails
                {
                    var alarmRecord = AlarmModel()
                    alarmRecord.objectId = alarm.valueForKey("objectId") as! String
                    alarmRecord.time = alarm.valueForKey("time") as! NSDate
                    alarmRecord.danceRoutine = alarm.valueForKey("danceRoutine") as! String
                    alarmRecord.evilMode = alarm.valueForKey("evilMode") as! Bool
                    alarmRecord.limitSnooze = alarm.valueForKey("limitSnooze") as! Int
                    alarmRecord.alarmTone = alarm.valueForKey("alarmTone") as! String
                    alarmRecord.repeat = alarm.valueForKey("repeat") as! Bool
                    alarmRecord.label = alarm.valueForKey("label") as! String
                    alarmRecord.enable = alarm.valueForKey("enable") as! Bool
                    alarmRecord.stepCount = alarm.valueForKey("stepCount") as! Int
                    alarmAll.append(alarmRecord)
                }
            } else {
                println("No alarms in db")
            }
        }
        return alarmAll
    }
    
    func getAlarmData (objectId : String) -> AlarmModel
    {
        var alarmRecord = AlarmModel()
        let request = NSFetchRequest()
        request.entity = DBAlarmHandler.entityDescription
        
        let predWorld = NSPredicate(format: "(objectId = %@)", objectId)
        request.predicate = predWorld
        
        var error: NSError?
        var objects = AppDelegate.DBManagedObjectContext?.executeFetchRequest(request, error: &error)
        if let alarmDetails = objects {
            if alarmDetails.count > 0 {
                alarmRecord.objectId = alarmDetails[0].valueForKey("objectId") as! String
                alarmRecord.time = alarmDetails[0].valueForKey("time") as! NSDate
                alarmRecord.danceRoutine = alarmDetails[0].valueForKey("danceRoutine") as! String
                alarmRecord.evilMode = alarmDetails[0].valueForKey("evilMode") as! Bool
                alarmRecord.limitSnooze = alarmDetails[0].valueForKey("limitSnooze") as! Int
                alarmRecord.alarmTone = alarmDetails[0].valueForKey("alarmTone") as! String
                alarmRecord.repeat = alarmDetails[0].valueForKey("repeat") as! Bool
                alarmRecord.label = alarmDetails[0].valueForKey("label") as! String
                alarmRecord.enable = alarmDetails[0].valueForKey("enable") as! Bool
                alarmRecord.stepCount = alarmDetails[0].valueForKey("stepCount") as! Int
            } else {
                println("No Data")
            }
        }
        return alarmRecord
    }
    func updateAlarmState(alarm: AlarmModel) -> Bool
    {
        
        var fetchRequest = NSFetchRequest(entityName: DBAlarmHandler.entityName)
        fetchRequest.entity = DBAlarmHandler.entityDescription
        fetchRequest.predicate = NSPredicate(format: "(objectId = %@)", alarm.objectId)
        
        if let fetchResults = AppDelegate.DBManagedObjectContext!.executeFetchRequest(fetchRequest, error: nil) as? [NSManagedObject] {
            if fetchResults.count != 0{
                
                var managedObject = fetchResults[0]
                managedObject.setValue(alarm.time, forKey: "time")
                managedObject.setValue(alarm.danceRoutine, forKey: "danceRoutine")
                managedObject.setValue(alarm.evilMode, forKey: "evilMode")
                managedObject.setValue(alarm.limitSnooze, forKey: "limitSnooze")
                managedObject.setValue(alarm.alarmTone, forKey: "alarmTone")
                managedObject.setValue(alarm.repeat, forKey: "repeat")
                managedObject.setValue(alarm.label, forKey: "label")
                managedObject.setValue(alarm.enable, forKey: "enable")
                managedObject.setValue(alarm.stepCount, forKey: "stepCount")
                
                AppDelegate.DBManagedObjectContext!.save(nil)
            }
        }
        return true
    }
    func checkIfAlarmExists(objectId: String) -> Bool
    {
        var count_flag : Bool = false
        let request = NSFetchRequest()
        request.entity = DBAlarmHandler.entityDescription
        let predAlarm = NSPredicate(format: "(objectId = %@)", objectId)
        request.predicate = predAlarm
        
        var error: NSError?
        var objects = AppDelegate.DBManagedObjectContext?.executeFetchRequest(request, error: &error)
        if let alarmDetails = objects {
            if alarmDetails.count > 0 {
                count_flag = true
            } else {
                println("No Data")
                return false
            }
        }
        return count_flag
    }
    func deleteAlarm(objectId: String) -> Bool
    {
        var statusFlag : Bool = false
        let request = NSFetchRequest()
        request.entity = DBAlarmHandler.entityDescription
        
        let predWorld = NSPredicate(format: "(objectId = %@)", objectId)
        request.predicate = predWorld
        
        var error: NSError?
        var objects = AppDelegate.DBManagedObjectContext?.executeFetchRequest(request, error: &error)
        if let alarmDetails = objects {
            if alarmDetails.count > 0 {
                AppDelegate.DBManagedObjectContext?.deleteObject(alarmDetails[0] as! NSManagedObject)
                statusFlag = true
            } else {
                println("No Data")
            }
        }
        return statusFlag
    }
    func deleteAllAlarms() -> Bool
    {
        var statusFlag : Bool = false
        let request = NSFetchRequest()
        request.entity = DBAlarmHandler.entityDescription
        
        var error: NSError?
        var objects = AppDelegate.DBManagedObjectContext?.executeFetchRequest(request, error: &error)
        if let alarmDetails = objects {
            if alarmDetails.count > 0 {
                AppDelegate.DBManagedObjectContext?.deleteObject(alarmDetails[0] as! NSManagedObject)
                statusFlag = true
            } else {
                println("No Data")
            }
        }
        return statusFlag
    }
}
