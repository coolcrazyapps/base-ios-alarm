//
//  DatabaseController.swift
//  AlarmClock
//
//  Created by Manpreet Singh Bedi on 29/06/15.
//  Copyright (c) 2015 Bazzinga Labs. All rights reserved.
//

import Foundation

class DatabaseController
{
    static let databaseController = DatabaseController()
    
    func createAlarmRecord(alarm: AlarmModel)
    {
        DBAlarmHandler.dbAlarmHandler.createAlarmRecord(alarm)
    }
    
    func getAlarmAllData() -> [AlarmModel]
    {
        return DBAlarmHandler.dbAlarmHandler.getAllData()
    }
    
    func getAlarmData(objectId : String) -> AlarmModel
    {
        return DBAlarmHandler.dbAlarmHandler.getAlarmData(objectId)
    }
    func updateAlarmState(alarm: AlarmModel) -> Bool
    {
        return DBAlarmHandler.dbAlarmHandler.updateAlarmState(alarm)
    }
    func checkIfAlarmExists(objectId: String) -> Bool
    {
        return DBAlarmHandler.dbAlarmHandler.checkIfAlarmExists(objectId)
    }
    func deleteAlarm(objectId: String) -> Bool
    {
        return DBAlarmHandler.dbAlarmHandler.deleteAlarm(objectId)
    }
    func deleteAllAlarms() -> Bool
    {
        return DBAlarmHandler.dbAlarmHandler.deleteAllAlarms()
    }
}
