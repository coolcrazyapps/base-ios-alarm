//
//  DeleteAlarmTableViewCell.swift
//  AlarmClock
//
//  Created by Mohammad Shabaz Moosa on 28/06/15.
//  Copyright (c) 2015 Bazzinga Labs. All rights reserved.
//

import UIKit

class DeleteAlarmTableViewCell: UITableViewCell {

    @IBOutlet weak var descriptionLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupCell(label: String){
        descriptionLabel.text = label
        Themes.themes.initDeleteAlarmTableViewCell(self as DeleteAlarmTableViewCell)

    }

}
