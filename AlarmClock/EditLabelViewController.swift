//
//  EditLabelViewController.swift
//  AlarmClock
//
//  Created by Mohammad Shabaz Moosa on 29/06/15.
//  Copyright (c) 2015 Bazzinga Labs. All rights reserved.
//

import UIKit

class EditLabelViewController: UIViewController {
    var setting: SettingModel?
    var delegate: SettingDetailChangeDelegate?

    @IBOutlet weak var labelTextField: DesignableTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        labelTextField.text = setting?.value as! String
        labelTextField.becomeFirstResponder()
        Themes.themes.initEditLabelViewController(self as EditLabelViewController)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        setting?.value = labelTextField.text
        delegate?.onSettingDetailChanged(setting!, sender: self)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
