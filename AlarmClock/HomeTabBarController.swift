//
//  HomeTabBarController.swift
//  AlarmClock
//
//  Created by Mohammad Shabaz Moosa on 29/06/15.
//  Copyright (c) 2015 Bazzinga Labs. All rights reserved.
//
import UIKit

class HomeTabBarController: UITabBarController {
    override func viewWillAppear(animated: Bool) {
        self.selectedIndex = 1
        UITabBar.appearance().barTintColor = UIColor(red: 92.0/255.0, green: 89.0/255.0, blue: 89.0/255.0, alpha: 1.0)
        UITabBar.appearance().tintColor = UIColor.whiteColor()
    }
}
