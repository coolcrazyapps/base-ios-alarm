//
//  LabelSettingTableViewCell.swift
//  AlarmClock
//
//  Created by Mohammad Shabaz Moosa on 28/06/15.
//  Copyright (c) 2015 Bazzinga Labs. All rights reserved.
//

import UIKit

class LabelSettingTableViewCell: UITableViewCell {
    var id: String!

    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupCell(id: String, label: String, value: String){
        self.id = id
        descriptionLabel.text = label
        valueLabel.text = value
        Themes.themes.initLabelSettingTableViewCell(self as LabelSettingTableViewCell)
    }

}
