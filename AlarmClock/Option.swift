//
//  Option.swift
//  AlarmClock
//
//  Created by Mohammad Shabaz Moosa on 30/06/15.
//  Copyright (c) 2015 Bazzinga Labs. All rights reserved.
//

import Foundation

class Option {
    var id: String = ""
    var name: String = ""
    var value: AnyObject!
    var type: String = ""
    
    init(id: String, name: String, value: AnyObject, type: String){
        self.id = id
        self.name = name
        self.value = value
        self.type = type
    }

}