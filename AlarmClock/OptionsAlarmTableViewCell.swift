//
//  OptionsAlarmTableViewCell.swift
//  AlarmClock
//
//  Created by Manpreet Singh Bedi on 11/07/15.
//  Copyright (c) 2015 Bazzinga Labs. All rights reserved.
//

import UIKit

class OptionsAlarmTableViewCell: UITableViewCell {
    
    @IBOutlet weak var descriptionLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setupCell(label: String){
        descriptionLabel.text = label
        Themes.themes.initOptionsAlarmTableViewCell(self as OptionsAlarmTableViewCell)
    }
    
}
