//
//  ParentalGate.swift
//  AlarmClock
//
//  Created by Manpreet Singh Bedi on 12/07/15.
//  Copyright (c) 2015 Bazzinga Labs. All rights reserved.
//

import UIKit
import QuartzCore
import Foundation

class ParentalGate: UIViewController{
    
    @IBOutlet weak var popUpView: UIView!
    @IBOutlet weak var firstNumber: UILabel!
    @IBOutlet weak var secondNumber: UILabel!
    @IBOutlet weak var answer: UITextField!
    @IBOutlet weak var arithematicOperator: UILabel!
    
    var parentStatus : ParentalGateListenerDelegate?
    var first : Int = 0
    var second : Int = 0
    var operations : Int = 0
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(nibName nibNameOrNil: String!, bundle nibBundleOrNil: NSBundle!) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.6)
        self.popUpView.layer.cornerRadius = 5
        self.popUpView.layer.shadowOpacity = 0.8
        first = generateRandomNumber(10)
        second = generateRandomNumber(10)
        operations = generateRandomNumber(1)
        self.popUpView.layer.shadowOffset = CGSizeMake(0.0, 0.0)
        self.firstNumber.text = String(first)
        self.secondNumber.text = String(second)
        self.arithematicOperator.text = numberToOperation(operations)
    }
    
    func showInView(aView: UIView!, animated: Bool)
    {
        aView.addSubview(self.view)
        if animated
        {
            self.showAnimate()
        }
    }
    
    func showAnimate()
    {
        self.view.transform = CGAffineTransformMakeScale(1.3, 1.3)
        self.view.alpha = 0.0;
        UIView.animateWithDuration(0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransformMakeScale(1.0, 1.0)
        });
    }
    
    func removeAnimate()
    {
        UIView.animateWithDuration(0.25, animations: {
            self.view.transform = CGAffineTransformMakeScale(1.3, 1.3)
            self.view.alpha = 0.0;
            }, completion:{(finished : Bool)  in
                if (finished)
                {
                    self.view.removeFromSuperview()
                }
        });
    }
    
    @IBAction func closePopup(sender: AnyObject) {
        self.removeAnimate()
    }
    @IBAction func submitButton(sender: AnyObject) {
        
        var userSolution : Int = answer.text.toInt()!
        var solution = 0
        switch operations
        {
        case 0:
            solution = first + second
            break
        case 1 :
            solution = first - second
            break
        case 2 :
            solution = first * second
            break
        default :
            break
        }
        if(solution == userSolution)
        {
            parentStatus!.parentApproved()
            self.removeAnimate()
        }
    }
    func generateRandomNumber(number : Int) -> Int
    {
        var random = Int(arc4random_uniform(UInt32(number)))
        return random
    }
    func numberToOperation(number : Int) -> String
    {
        switch number
        {
        case 0:
            return "+"
        case 1 :
            return "-"
        case 2 :
            return "x"
        default :
            return ""
        }
    }
}
