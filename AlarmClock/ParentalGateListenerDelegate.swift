//
//  ParentalGateListenerDelegate.swift
//  AlarmClock
//
//  Created by Manpreet Singh Bedi on 12/07/15.
//  Copyright (c) 2015 Bazzinga Labs. All rights reserved.
//

import Foundation
protocol ParentalGateListenerDelegate {
    func parentApproved ()
}