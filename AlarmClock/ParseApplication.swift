//
//  ParseApplication.swift
//  AlarmClock
//
//  Created by Manpreet Singh Bedi on 03/07/15.
//  Copyright (c) 2015 Bazzinga Labs. All rights reserved.
//

import Foundation
import Parse
import Bolts

class ParseApplication
{
    static let parseApplication = ParseApplication()
    
    func initParse(didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?)
    {
        parseInitialize(didFinishLaunchingWithOptions: launchOptions)
        parseEmail()
    }
    
    func parseInitialize(didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?)
    {
        
        Parse.setApplicationId("H3i9T4u3BlOCOrKcyGbbs4HQ8oQ4TsNdCgNFQ9Em",clientKey: "S27Py5ST7Gx5l43itgfHZBsMSfvcFvmhs0xCO59g")
        PFAnalytics.trackAppOpenedWithLaunchOptions(launchOptions)
        
        PFACL.setDefaultACL(PFACL(), withAccessForCurrentUser: true)
        let installation: PFInstallation = PFInstallation.currentInstallation()
        installation["user"] = self.getEmail()
        installation.saveInBackground();
    }
    
    func parseEmail()
    {
        let possibleEmail : String = self.getEmail()
        PFUser.logInWithUsernameInBackground(possibleEmail, password: "password") {
            (user: PFUser?, error: NSError?) -> Void in
            if error == nil {
                let object_id : String = PFUser.currentUser()!.objectId!
                let userReferralCode = object_id.substringWithRange(Range<String.Index>(start: advance(object_id.endIndex,-5) , end: advance(object_id.endIndex, 0) ))
            } else {
                println(error)
            }
        }
        
        let currentUser = PFUser.currentUser()
        if(currentUser == nil)
        {
            var user = PFUser()
            user.email = self.getEmail()+"@gmail.com"
            user.password = "password"
            user.username = self.getEmail()
            user.signUpInBackgroundWithBlock{(success: Bool, error: NSError?) -> Void in
                if error == nil  {
                    let object_id : String? = PFUser.currentUser()?.objectId!
                    let userReferralCode = object_id!.substringWithRange(Range<String.Index>(start: advance(object_id!.endIndex,-5) , end: advance(object_id!.endIndex, 0) ))
                }
                else
                {
                    println(error?.description)
                }
            }
        }
    }
    
    func parseEvents(key: String, value: String, event:String) {
        PFAnalytics.trackEventInBackground(event, dimensions : [key : value]  ,block: nil)
    }
    
    func parseManageLogin(userID: String) {
        let currentUser: PFUser = PFUser.currentUser()!
        if(currentUser.email != nil)
        {
            currentUser["User ID"] = userID
            currentUser.signUpInBackground()
        }
    }
    
    func getEmail() -> String
    {
        return UIDevice.currentDevice().identifierForVendor.UUIDString
    }
    
}
