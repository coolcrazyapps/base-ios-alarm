//
//  SelectOptionTableViewController.swift
//  AlarmClock
//
//  Created by Mohammad Shabaz Moosa on 30/06/15.
//  Copyright (c) 2015 Bazzinga Labs. All rights reserved.
//

import UIKit
import AVFoundation

class SelectOptionTableViewController: UITableViewController, UITableViewDelegate, UITableViewDataSource {
    var options: [Option] = []
    var selectedOption: Option!
    var delegate: SelectedOptionChangeDelegate!
    var leftBarButton: UIBarButtonItem?
    var audioPlayer: AVAudioPlayer!
    var isInitialSelection: Bool = true
    
    override func viewDidLoad() {
        if isAlarmToneSelection() {
            self.audioPlayer = AVAudioPlayer()
        }
        
        self.tableView.tableFooterView = UIView()
        
    }
    
    func isAlarmToneSelection() -> Bool {
        return self.title == "Alarm Tone"
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return options.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("OptionCell") as! UITableViewCell
        cell.textLabel?.text = options[indexPath.row].name
        if(selectedOption.id == options[indexPath.row].id) {
            cell.accessoryType = UITableViewCellAccessoryType.Checkmark
        }
        else {
            cell.accessoryType = UITableViewCellAccessoryType.None
        }
        Themes.themes.initOptionsItemTableViewCell(cell)
        return cell
    }
    
    
    func playAlarmSound(option: Option) {
        var alarmToneTemp = option.value as! String
        var alarmTone = alarmToneTemp.substringWithRange(Range<String.Index>(start: alarmToneTemp.startIndex, end: advance(alarmToneTemp.endIndex, -4)))
        println(alarmTone)
        var alertSound = NSURL(fileURLWithPath: NSBundle.mainBundle().pathForResource(alarmTone, ofType: "mp3")!)
        var error:NSError?
        audioPlayer = AVAudioPlayer(contentsOfURL: alertSound, error: &error)
        audioPlayer.numberOfLoops = 1
        audioPlayer.prepareToPlay()
        audioPlayer.play()
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        var currentOption = options[indexPath.row]

        // Block premium tones
        if (isAlarmToneSelection() && Utilities.isPremiumTone(currentOption) && !StoreHelper.isTonesUnlocked()) {
            var blockedAlert = UIAlertController(title: "Locked", message: "Please buy premium tones pack from the shop to use this", preferredStyle: UIAlertControllerStyle.Alert)
            
            blockedAlert.addAction(UIAlertAction(title: "OK", style: .Default, handler: { (action: UIAlertAction!) in
                
            }))
            presentViewController(blockedAlert, animated: true, completion: nil)
            
        }
        else {
            selectedOption = options[indexPath.row]
        }
        delegate.onSelectedOptionChanged(selectedOption)
        if isAlarmToneSelection() {
            playAlarmSound(currentOption)
        }
        tableView.reloadData()
    }
    
}
