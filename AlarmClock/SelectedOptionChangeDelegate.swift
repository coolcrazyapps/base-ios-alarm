//
//  SelectedOptionChangeDelegate.swift
//  AlarmClock
//
//  Created by Mohammad Shabaz Moosa on 30/06/15.
//  Copyright (c) 2015 Bazzinga Labs. All rights reserved.
//

protocol SelectedOptionChangeDelegate: class {
    func onSelectedOptionChanged(option: Option)
}
