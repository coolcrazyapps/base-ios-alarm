//
//  SettingDetailChangeDelegate.swift
//  AlarmClock
//
//  Created by Mohammad Shabaz Moosa on 29/06/15.
//  Copyright (c) 2015 Bazzinga Labs. All rights reserved.
//

import Foundation

protocol SettingDetailChangeDelegate: class {
    func onSettingDetailChanged(setting: SettingModel, sender: AnyObject)
}