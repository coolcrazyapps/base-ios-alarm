//
//  SettingsListViewController.swift
//  AlarmClock
//
//  Created by Manpreet Singh Bedi on 09/07/15.
//  Copyright (c) 2015 Bazzinga Labs. All rights reserved.
//

import UIKit
import MessageUI
import iAd

extension String {
    func toBool() -> Bool{
        switch self {
        case "True", "true", "yes", "1":
            return true
        case "False", "false", "no", "0":
            return false
        default:
            return false
        }
    }
}

class SettingsListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, SwitchSettingTableViewCellDelegate, SettingDetailChangeDelegate, SelectedOptionChangeDelegate, MFMailComposeViewControllerDelegate, ADBannerViewDelegate {
    var evilMode : Bool = true
    
    // setting defaults
    var snoozeDuration = Constants.DEFAULT_SNOOZE_DURATION
    var snoozeLimit : Int = Constants.DEFAULT_SNOOZE_LIMIT
    var alarmTone : String = Constants.DEFAULT_ALARM_TONE
    var stepCount: Int = Constants.DEFAULT_STEP_COUNT
    var stepSensitivity: Float = Constants.DEFAULT_STEP_SENSITIVITY
    var shakeSensitivity: Float = Constants.DEFAULT_SHAKE_SENSITIVITY

    var activeSetting: SettingModel?
    var delegate : AlarmDetailChangeDelegate?
    var rectangleAdView: ADBannerView?
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setupSettings()
        self.tableView.tableFooterView = UIView()
        if(!StoreHelper.isAdsRemoved())
        {
            initBannerAds()
        }
        Themes.themes.initSettingsListViewController(self)
    }
    
    override func viewWillAppear(animated: Bool) {
        // time picker needs to be set separately as its not part of tableview
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupSettings()
    {
        if(SharedPreferences.preferences.checkKey(Constants.ALARM_EVIL_MODE))
        {
            evilMode = SharedPreferences.preferences.getKeyValue(Constants.ALARM_EVIL_MODE).toBool()
        }
        if(SharedPreferences.preferences.checkKey(Constants.ALARM_SNOOZE_DURATION))
        {
            snoozeDuration = SharedPreferences.preferences.getValueOrDefault(Constants.ALARM_SNOOZE_DURATION, defaultValue: Constants.DEFAULT_SNOOZE_DURATION) as! Int
        }
        if(SharedPreferences.preferences.checkKey(Constants.ALARM_SNOOZE_LIMIT))
        {
            snoozeLimit = SharedPreferences.preferences.getValueOrDefault(Constants.ALARM_SNOOZE_LIMIT, defaultValue: Constants.DEFAULT_SNOOZE_LIMIT) as! Int
        }
        if(SharedPreferences.preferences.checkKey(Constants.ALARM_TONE))
        {
            alarmTone = SharedPreferences.preferences.getKeyValue(Constants.ALARM_TONE)
        }
        if(SharedPreferences.preferences.checkKey(Constants.ALARM_STEP_COUNT))
        {
            stepCount = SharedPreferences.preferences.getValueOrDefault(Constants.ALARM_STEP_COUNT, defaultValue: Constants.DEFAULT_STEP_COUNT) as! Int
        }
        if(SharedPreferences.preferences.checkKey(Constants.STEP_SENSITIVITY))
        {
            stepSensitivity = SharedPreferences.preferences.getValueOrDefault(Constants.STEP_SENSITIVITY, defaultValue: Constants.DEFAULT_STEP_SENSITIVITY) as! Float
        }
        if(SharedPreferences.preferences.checkKey(Constants.SHAKE_SENSITIVITY))
        {
            shakeSensitivity = SharedPreferences.preferences.getValueOrDefault(Constants.SHAKE_SENSITIVITY, defaultValue: Constants.DEFAULT_SHAKE_SENSITIVITY) as! Float
        }
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let  headerCell = tableView.dequeueReusableCellWithIdentifier("CustomHeaderCell") as! CustomHeaderTableViewCell
        switch section {
        case 1:
            headerCell.setupCell("Default Alarm Settings")
            return headerCell
        case 0:
            headerCell.setupCell("Step Counter Settings")
            return headerCell
        case 2:
            headerCell.setupCell("Other Options")
            return headerCell
        default:
            headerCell.setupCell("Unknown section")
            return headerCell
        }
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        switch indexPath.section {
        case 1:
            switch indexPath.row {
            case 0:
                let cell = tableView.dequeueReusableCellWithIdentifier("SwitchSettingCell", forIndexPath: indexPath) as! SwitchSettingTableViewCell
                cell.setupCell(CellId.EvilMode, label: "Evil Mode", value: evilMode, delegate: self)
                return cell
            case 1:
                let cell = tableView.dequeueReusableCellWithIdentifier("LabelSettingCell", forIndexPath: indexPath) as! LabelSettingTableViewCell
                cell.setupCell(CellId.SnoozeDuration, label: "Snooze Duration", value: Utilities.valueToDisplayForOption(snoozeDuration, options: Utilities.getSnoozeDurationOptions()))
                return cell
            case 2:
                let cell = tableView.dequeueReusableCellWithIdentifier("LabelSettingCell", forIndexPath: indexPath) as! LabelSettingTableViewCell
                cell.setupCell(CellId.SnoozeLimit, label: "Snooze Limit", value: Utilities.valueToDisplayForOption(snoozeLimit, options: Utilities.getSnoozeOptions()))
                return cell
            case 3:
                let cell = tableView.dequeueReusableCellWithIdentifier("LabelSettingCell", forIndexPath: indexPath) as! LabelSettingTableViewCell
                cell.setupCell(CellId.AlarmTone, label: "Alarm Tone", value: Utilities.valueToDisplayForOption(alarmTone, options: Utilities.getAlarmToneOptions()))
                return cell
            default:
                return UITableViewCell()
            }
        case 0:
            switch indexPath.row {
            case 0:
                let cell = tableView.dequeueReusableCellWithIdentifier("LabelSettingCell", forIndexPath: indexPath) as! LabelSettingTableViewCell
                cell.setupCell(CellId.StepCount, label: "Number Of Steps", value: Utilities.valueToDisplayForOption(stepCount, options: Utilities.getStepCountOptions()))
                return cell
            case 1:
                let cell = tableView.dequeueReusableCellWithIdentifier("LabelSettingCell", forIndexPath: indexPath) as! LabelSettingTableViewCell
                cell.setupCell(CellId.StepSensitivity, label: "Step Sensitivity", value: Utilities.valueToDisplayForOption(stepSensitivity, options: Utilities.getStepSensitivityOptions()))
                return cell
            case 2:
                let cell = tableView.dequeueReusableCellWithIdentifier("LabelSettingCell", forIndexPath: indexPath) as! LabelSettingTableViewCell
                cell.setupCell(CellId.ShakeSensitivity, label: "Shake Sensitivity", value: Utilities.valueToDisplayForOption(shakeSensitivity, options: Utilities.getShakeSensitivityOptions()))
                return cell
            default:
                return UITableViewCell()
        }
        case 2:
            switch indexPath.row {
            case 0:
                let cell = tableView.dequeueReusableCellWithIdentifier("OptionsAlarmCell") as! OptionsAlarmTableViewCell
                cell.setupCell("Rate & Review")
                return cell
            case 1:
                let cell = tableView.dequeueReusableCellWithIdentifier("OptionsAlarmCell") as! OptionsAlarmTableViewCell
                cell.setupCell("Email")
                return cell
            case 2:
                let cell = tableView.dequeueReusableCellWithIdentifier("DeleteAlarmCell") as! DeleteAlarmTableViewCell
                cell.setupCell("Delete All Alarms")
                return cell
            default:
                return UITableViewCell()
            }
        default:
            return UITableViewCell()
        }
    }
    
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 1: return 4
        case 0: return 3
        case 2: return 3
        default: return 1
        }
    }
    
    // MARK: Handle cell interaction
    func switchSettingOnValueChanged(cell: SwitchSettingTableViewCell, sender: UISwitch) {
        switch cell.id {
        case CellId.EvilMode:
            evilMode = cell.settingSwitch.on
            SharedPreferences.preferences.putKey(Constants.ALARM_EVIL_MODE, value: String(stringInterpolationSegment: evilMode))
        default: println("Unknown id \(cell.id)")
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        switch indexPath.section {
        case 1:
            switch indexPath.row {
            case 1:
                // Alarm Tone
                activeSetting = SettingModel(id: CellId.SnoozeDuration, name: "Snooze Duration", value: String(snoozeDuration), type: "string")
                performSegueWithIdentifier("SelectOptionSegue", sender: self)
            case 2:
                // Snooze Duration
                if(StoreHelper.isSnoozeLimitUnlocked())
                {
                    activeSetting = SettingModel(id: CellId.SnoozeLimit, name: "Snooze Limit", value: String(snoozeLimit), type: "number")
                    performSegueWithIdentifier("SelectOptionSegue", sender: self)
                }
                else
                {
                    var deleteAlert = UIAlertController(title: "Buy Snooze Limit", message: "Purchase this feature to limit number of snoozes.", preferredStyle: UIAlertControllerStyle.Alert)
                    deleteAlert.addAction(UIAlertAction(title: "OK", style: .Default, handler: nil))
                    presentViewController(deleteAlert, animated: true, completion: nil)
                }
            case 3:
                // Snooze Limit
                activeSetting = SettingModel(id: CellId.AlarmTone, name: "Alarm Tone", value: String(alarmTone), type: "string")
                performSegueWithIdentifier("SelectOptionSegue", sender: self)
            default: println("unknown")
            }
        case 0:
            switch indexPath.row {
            case 0:
                activeSetting = SettingModel(id: CellId.StepCount, name: "Number Of Steps", value: String(stepCount), type: "number")
                performSegueWithIdentifier("SelectOptionSegue", sender: self)
                
            case 1:
                activeSetting = SettingModel(id: CellId.StepSensitivity, name: "Step Sensitivity", value: stepSensitivity, type: "float")
                performSegueWithIdentifier("SelectOptionSegue", sender: self)
                
            case 2:
                activeSetting = SettingModel(id: CellId.ShakeSensitivity, name: "Shake Sensitivity", value: shakeSensitivity, type: "float")
                performSegueWithIdentifier("SelectOptionSegue", sender: self)
            default: println("unknown")
            }
        case 2:
            switch indexPath.row {
            case 0:
                let url = NSURL(string: Constants.APP_URL)
                UIApplication.sharedApplication().openURL(url!)
                break
            case 1:
                var toRecipents = Constants.MAIL_RECIPIENT
                var mc: MFMailComposeViewController = MFMailComposeViewController()
                mc.mailComposeDelegate = self
                mc.setSubject("Alarm Clock")
                mc.setToRecipients(toRecipents)
                self.presentViewController(mc, animated: true, completion: nil)
                break
            case 2:
                confirmDeletion()
                break
            default: println("unknown")
            }
        default: println("Unknown selection")
        }
    }
    
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "TextInputSegue" {
            let destinationVC = segue.destinationViewController as! EditLabelViewController
            destinationVC.setting = activeSetting
            destinationVC.delegate = self
        }
        else if segue.identifier == "SelectOptionSegue" {
            var options : [Option] = []
            var selectedOption : Option = Option(id: "" ,name: "" ,value: "" ,type: "")
            if(activeSetting?.name == "Snooze Duration")
            {
                options = Utilities.getSnoozeDurationOptions()
                selectedOption = Utilities.getSelectedOption(snoozeDuration, options: options)
            }
            else if(activeSetting?.name == "Snooze Limit")
            {
                options = Utilities.getSnoozeOptions()
                selectedOption = Utilities.getSelectedOption(snoozeLimit, options: options)
            }
            else if (activeSetting?.name == "Alarm Tone")
            {
                options = Utilities.getAlarmToneOptions()
                selectedOption = Utilities.getSelectedOption(alarmTone, options: options)
            }
            else if (activeSetting?.id == CellId.StepCount)
            {
                options = Utilities.getStepCountOptions()
                selectedOption = Utilities.getSelectedOption(stepCount, options: options)
            }
            else if (activeSetting?.id == CellId.StepSensitivity)
            {
                options = Utilities.getStepSensitivityOptions()
                selectedOption = Utilities.getSelectedOption(stepSensitivity, options: options)
            }
            else if (activeSetting?.id == CellId.ShakeSensitivity)
            {
                options = Utilities.getShakeSensitivityOptions()
                selectedOption = Utilities.getSelectedOption(shakeSensitivity, options: options)
            }
            let destinationVC = segue.destinationViewController as! SelectOptionTableViewController
            destinationVC.options = options
            destinationVC.title = activeSetting?.name
            destinationVC.selectedOption = selectedOption
            destinationVC.delegate = self
            
        }
    }
    
    
    func onSettingDetailChanged(setting: SettingModel, sender: AnyObject) {
        tableView.reloadData()
    }
    
    func onSelectedOptionChanged(option: Option) {
        if activeSetting?.id == CellId.SnoozeDuration {
            snoozeDuration = option.value as! Int
            SharedPreferences.preferences.putKey(Constants.ALARM_SNOOZE_DURATION, value: snoozeDuration)
        }
        else if activeSetting?.id == CellId.SnoozeLimit {
            snoozeLimit = option.value as! Int
            SharedPreferences.preferences.putKey(Constants.ALARM_SNOOZE_LIMIT, value: snoozeLimit)
        }
        else if activeSetting?.id == CellId.AlarmTone {
            alarmTone = option.value as! String
            SharedPreferences.preferences.putKey(Constants.ALARM_TONE, value: alarmTone)
        }
        else if activeSetting?.id == CellId.StepCount {
            stepCount = option.value as! Int
            SharedPreferences.preferences.putKey(Constants.ALARM_STEP_COUNT, value: stepCount)
        }
        else if activeSetting?.id == CellId.StepSensitivity {
            stepSensitivity = option.value as! Float
            SharedPreferences.preferences.putKey(Constants.STEP_SENSITIVITY, value: stepSensitivity)
        }
        else if activeSetting?.id == CellId.ShakeSensitivity {
            shakeSensitivity = option.value as! Float
            SharedPreferences.preferences.putKey(Constants.SHAKE_SENSITIVITY, value: shakeSensitivity)
        }
        
        tableView.reloadData()
    }
    func confirmDeletion(){
        var deleteAlert = UIAlertController(title: "Delete Alarm", message: "Are you sure you want to delete the alarm?", preferredStyle: UIAlertControllerStyle.Alert)
        
        deleteAlert.addAction(UIAlertAction(title: "Yes", style: .Default, handler: { (action: UIAlertAction!) in
            UIApplication.sharedApplication().cancelAllLocalNotifications()
            DatabaseController.databaseController.deleteAllAlarms()
            JLToast.makeText("All Alarms Deleted ...").show()
        }))
        
        deleteAlert.addAction(UIAlertAction(title: "Cancel", style: .Default, handler: nil))
        
        presentViewController(deleteAlert, animated: true, completion: nil)
    }
    func mailComposeController(controller:MFMailComposeViewController, didFinishWithResult result:MFMailComposeResult, error:NSError) {
        switch result.value {
        case MFMailComposeResultCancelled.value:
            println("Mail cancelled")
        case MFMailComposeResultSaved.value:
            println("Mail saved")
        case MFMailComposeResultSent.value:
            println("Mail sent")
        case MFMailComposeResultFailed.value:
            println("Mail sent failure: %@", [error.localizedDescription])
        default:
            break
        }
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    /* Display Banner Ads : Init */
    func initBannerAds()
    {
        self.canDisplayBannerAds = true
        rectangleAdView = ADBannerView(adType: ADAdType.Banner)
        rectangleAdView?.delegate = self
    }
    
    func bannerViewDidLoadAd(banner: ADBannerView!) {
        self.view.addSubview(banner)
        self.view.layoutIfNeeded()
    }
    
    func bannerView(banner: ADBannerView!, didFailToReceiveAdWithError error: NSError!) {
        banner.removeFromSuperview()
        self.view.layoutIfNeeded()
    }
    /* Display Banner Ads : End */

}