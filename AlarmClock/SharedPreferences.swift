//
//  SharedPreferences.swift
//  AlarmClock
//
//  Created by Manpreet Singh Bedi on 04/07/15.
//  Copyright (c) 2015 Bazzinga Labs. All rights reserved.
//

import Foundation
class SharedPreferences
{
    static let preferences = SharedPreferences()
    static let sharedPreferences = NSUserDefaults.standardUserDefaults()
    func initSharedPreferences()
    {
        putKey(Constants.ALARM_EVIL_MODE, value: String(stringInterpolationSegment: true))
        putKey(Constants.ALARM_SNOOZE_DURATION, value: 2)
        putKey(Constants.ALARM_SNOOZE_LIMIT, value: -1)
        putKey(Constants.ALARM_TONE, value: "Alarm.mp3")
    }
    func getKeyValue (key : String) -> String
    {
        if (SharedPreferences.sharedPreferences.objectForKey(key) != nil)
        {
            return SharedPreferences.sharedPreferences.objectForKey(key) as! String
        }
        else
        {
            return ""
        }
    }
    
    func getValueOrDefault (key : String, defaultValue: AnyObject) -> AnyObject
    {
        if (SharedPreferences.sharedPreferences.objectForKey(key) != nil)
        {
            return SharedPreferences.sharedPreferences.objectForKey(key)!
        }
        else
        {
            return defaultValue
        }
    }
    
    func checkKey(key : String) -> Bool
    {
        if(SharedPreferences.sharedPreferences.objectForKey(key) != nil)
        {
            return true
        }
        else
        {
            return false
        }
    }
    
    func putKey(key : String , value : AnyObject)
    {
        SharedPreferences.sharedPreferences.setObject(value, forKey: key)
        SharedPreferences.sharedPreferences.synchronize()
    }
    
}

