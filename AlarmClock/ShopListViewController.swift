//
//  ShopListViewController.swift
//  AlarmClock
//
//  Created by Manpreet Singh Bedi on 03/07/15.
//  Copyright (c) 2015 Bazzinga Labs. All rights reserved.
//

import Foundation
import UIKit
import StoreKit
import iAd

class ShopListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, SKPaymentTransactionObserver, SKProductsRequestDelegate , ADBannerViewDelegate, ParentalGateListenerDelegate {
    
    var selectedProduct: SKProduct?
    var ID_LIMIT_SNOOZE = "com.bazzingalabs.limitsnooze"
    var ID_NO_ADS = "com.bazzingalabs.noads"
    var ID_RINGTONES = "com.bazzingalabs.ringtones"
    
    var limitSnoozeProduct : SKProduct?
    var noAdsProduct : SKProduct?
    var ringtonesProduct : SKProduct?
    var themesProduct : SKProduct?
    var products: [SKProduct] = []
    var isProductDetailsReceived: Bool = false
    
    var purchaseType : String = ""
    var indicator: UIActivityIndicatorView!
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var restoreButton: UIBarButtonItem!
    var storeItems = StoreHelper.getItemsAvailable()
    var rectangleAdView: ADBannerView?
    var parentalGate : ParentalGate!
    var parentStatus = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // remove extra lines
        self.tableView.tableFooterView = UIView()
        getProductInfo()
        if(!StoreHelper.isAdsRemoved())
        {
            initBannerAds()
        }
        Themes.themes.initShopListViewController(self as ShopListViewController)
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let storeItem = storeItems[indexPath.row]
        // fetch the store kit product
        var product = getProductById(storeItem.id)
        if let currentProduct = product {
            selectedProduct = currentProduct
            println("Initiating payment for \(storeItem.id)")
            let payment = SKPayment(product: selectedProduct)
            SKPaymentQueue.defaultQueue().addPayment(payment)
        }
        else  {
            JLToast.makeText("Please wait while we load the store items").show()
            println("\(storeItem.id) not found")
        }
    }
    
    func setupLoadingDialog() {
        indicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.Gray)
        indicator.frame = CGRectMake(0.0, 0.0, 40.0, 40.0)
        indicator.center = self.view.center
        self.view.addSubview(indicator)
        indicator.bringSubviewToFront(self.view)
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let storeItem = storeItems[indexPath.row]
        let cell = tableView.dequeueReusableCellWithIdentifier("StoreItemCell") as! StoreItemCell
        cell.titleLabel?.text = storeItem.title
        cell.descriptionLabel?.text = storeItem.description
        cell.itemImage?.image = UIImage(named: storeItem.imageName)
        Themes.themes.initStoreItemCell(cell)
        return cell
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return storeItems.count
    }
    
    func getProductById(id: String) -> SKProduct? {
        for product in products {
            if product.productIdentifier == id {
                return product
            }
        }
        
        return nil
    }
    
    func getProductInfo()
    {
        if SKPaymentQueue.canMakePayments() {
            let request = SKProductsRequest(productIdentifiers:
                NSSet(objects: self.ID_LIMIT_SNOOZE,self.ID_NO_ADS,self.ID_RINGTONES) as Set<NSObject>)
            request.delegate = self
            request.start()
        } else {
        }
    }
    func productsRequest(request: SKProductsRequest!, didReceiveResponse response: SKProductsResponse!) {
        
        products = response.products as! [SKProduct]
        if (products.count != 0) {
            isProductDetailsReceived = true
            tableView.reloadData()
        }
        var invalidProducts = response.invalidProductIdentifiers
        for product in invalidProducts
        {
            println("Product not found: \(product)")
        }
    }
    func paymentQueue(queue: SKPaymentQueue!, updatedTransactions transactions: [AnyObject]!) {
        
        for transaction in transactions as! [SKPaymentTransaction] {
            
            switch transaction.transactionState {
                
            case SKPaymentTransactionState.Purchased:
                println("DONE")
                var productId = transaction.payment.productIdentifier
                self.unlockFeature(productId)
                
                ParseApplication.parseApplication.parseEvents("Type", value: productId, event: "Buy")
                SKPaymentQueue.defaultQueue().finishTransaction(transaction)
                tableView.reloadData()
                break
            case SKPaymentTransactionState.Failed:
                println("FAIL")
                JLToast.makeText("Purchase failed").show()
                SKPaymentQueue.defaultQueue().finishTransaction(transaction)
                break
            default:
                break
            }
        }
    }
    func unlockFeature(productId: String) {
        StoreHelper.unlockItem(productId)
        storeItems = StoreHelper.getItemsAvailable()
        tableView.reloadData()
    }
    func paymentQueueRestoreCompletedTransactionsFinished(queue: SKPaymentQueue!) {
        var purchasedItemIDS = []
        for transaction:SKPaymentTransaction in queue.transactions as! [SKPaymentTransaction] {
            ParseApplication.parseApplication.parseEvents("Type", value: transaction.payment.productIdentifier, event: "Restore")
            unlockFeature(transaction.payment.productIdentifier)
        }
        var alert = UIAlertView(title: "Thank You", message: "Your purchase(s) were restored.", delegate: nil, cancelButtonTitle: "OK")
        alert.show()
    }
    @IBAction func restoreItemsButton(sender: UIBarButtonItem) {
        SKPaymentQueue.defaultQueue().addTransactionObserver(self)
        SKPaymentQueue.defaultQueue().restoreCompletedTransactions()
    }
    
    /* Display Banner Ads : Init */
    func initBannerAds()
    {
        self.canDisplayBannerAds = true
        rectangleAdView = ADBannerView(adType: ADAdType.Banner)
        rectangleAdView?.delegate = self
    }
    
    func bannerViewDidLoadAd(banner: ADBannerView!) {
        self.view.addSubview(banner)
        self.view.layoutIfNeeded()
    }
    
    func bannerView(banner: ADBannerView!, didFailToReceiveAdWithError error: NSError!) {
        banner.removeFromSuperview()
        self.view.layoutIfNeeded()
    }
    /* Display Banner Ads : End */
 
    func parentGateVisibility() {
        if(!parentStatus)
        {
            self.parentalGate = ParentalGate(nibName: "ParentalGate", bundle: nil)
            self.parentalGate.showInView(self.view, animated: true)
            self.parentalGate.parentStatus = self
            
        }
    }
    func parentApproved() {
        parentStatus = true
    }

}
