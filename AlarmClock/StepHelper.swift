//
//  StepHelper.swift
//  AlarmClock
//
//  Created by Mohammad Shabaz Moosa on 21/07/15.
//  Copyright (c) 2015 Bazzinga Labs. All rights reserved.
//

import CoreMotion

protocol StepDetectionDelegate: class {
    func onStep()
    func onShake()
}

class StepHelper {
    var manager: CMMotionManager!
    var delegate: StepDetectionDelegate!
    var timeCurrent: Double!
    var timeLast: Double!
    var timeDelta: Double!
    var accelLast: Double! = 0
    var accelCurrent: Double! = 0
    var accelMin: Double!
    var accelMax: Double!
    var xLast: Double! = 0
    var yLast: Double! = 0
    var zLast: Double! = 0
    var deltaMax: Double! = 0.7
    var cheatCount = 0
    var maxCheat = 2
    
    init() {
        self.manager = CMMotionManager()
        
        self.timeLast = now()
        self.accelMin = Double(Constants.DEFAULT_STEP_SENSITIVITY)
        self.accelMax = Double(Constants.DEFAULT_SHAKE_SENSITIVITY)
        
        setupAccelerometer()
    }
    
    func now() -> Double {
        return NSDate().timeIntervalSince1970 * 1000
    }
    
    func setupAccelerometer() {
        if manager.accelerometerAvailable {
            manager.accelerometerUpdateInterval = 0.01
            manager.startAccelerometerUpdatesToQueue(NSOperationQueue.mainQueue()) {
                [weak self] (data: CMAccelerometerData!, error: NSError!) in
                let me = self!
                var isDeltaMax = false
                
                me.timeCurrent = me.now()
                me.timeDelta = me.timeCurrent - me.timeLast;
                //NSLog(@"didAccelerate %f",timeDelta);
                
                let x = data.acceleration.x;
                let y = data.acceleration.y;
                let z = data.acceleration.z;
                
                var deltaX = fabs(x - me.xLast);
                var deltaY = fabs(y - me.yLast);
                var deltaZ = fabs(z - me.zLast);
                
                me.accelLast = me.accelCurrent
                me.accelCurrent =  sqrt(x*x + y*y + z*z);
                
                var accelDelta = fabs(me.accelCurrent - me.accelLast);
                
                //mAccel = (mAccel * 0.9f + delta);
                
                deltaX = fabs(me.xLast - x);
                deltaY = fabs(me.yLast - y);
                deltaZ = fabs(me.zLast - z);
                
                var accel = sqrt(deltaX*deltaX + deltaY * deltaY + deltaZ * deltaZ);
                //mAccel  = sqrt(x * x + y * y + z * z);
                //NSLog([NSString stringWithFormat:@"%f",mAccel]);
                
                //mAccelCurrent = mAccel;
                //mAccel = delta;
                
                /*
                if (deltaX < NOISE) deltaX = 0;
                if (deltaY < NOISE) deltaY = 0;
                if (deltaZ < NOISE) deltaZ = 0;
                */
                me.xLast = x;
                me.yLast = y;
                me.zLast = z;
                
                isDeltaMax = deltaX > me.deltaMax || deltaY > me.deltaMax || deltaZ > me.deltaMax
                
                if(accel > me.accelMin && accel <= me.accelMax && !isDeltaMax && me.timeDelta > 300)
                {
                    me.delegate.onStep()
                    me.timeLast = me.timeCurrent;
                    
                    if(me.cheatCount>0) {
                        me.cheatCount--;
                    }
                }
                
                if(isDeltaMax || accel > me.accelMax)
                {
                    //SHAKE detected
                    me.cheatCount++;
                    if(me.cheatCount == me.maxCheat)
                    {
                        me.cheatCount = 0;
                        me.delegate.onShake()
                    }
                }
                

                
            }
        }
    }
    
}