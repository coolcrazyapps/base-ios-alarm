//
//  StoreHelper.swift
//  AlarmClock
//
//  Created by Mohammad Shabaz Moosa on 07/07/15.
//  Copyright (c) 2015 Bazzinga Labs. All rights reserved.
//

import Foundation

class StoreHelper {
    static let ID_LIMIT_SNOOZE = "com.bazzingalabs.limitsnooze"
    static let ID_NO_ADS = "com.bazzingalabs.noads"
    static let ID_RINGTONES = "com.bazzingalabs.ringtones"
    
    static var storeItems = [
        StoreItem(id: ID_NO_ADS, title: "Remove Ads", description: "Remove the annoying ads", imageName: "no_ads", isOwned: false),
        StoreItem(id: ID_LIMIT_SNOOZE, title: "Limit snooze", description: "Limit the number of snoozes you can make", imageName: "limit_snooze", isOwned: false),
        StoreItem(id: ID_RINGTONES, title: "Tone Pack", description: "Get an awesome collection of tones to wake you up", imageName: "tone_pack", isOwned: false)
    ]
    
    static func getItemsAvailable()-> [StoreItem] {
        var availableItems: [StoreItem] = [];
        var sharedPreferences = SharedPreferences();
        
        for(var i = 0; i < storeItems.count; i++){
            if !sharedPreferences.checkKey(storeItems[i].id) {
                availableItems.append(storeItems[i])
            }
        }
        
        return availableItems
    }
    
    static func unlockItem(productId: String) {
        let sharedPreferences = SharedPreferences()
        sharedPreferences.putKey(productId, value: true)
    }
    static func isSnoozeLimitUnlocked() -> Bool {
        let sharedPreferences = SharedPreferences()
        return sharedPreferences.checkKey(ID_LIMIT_SNOOZE);
    }
    
    static func isTonesUnlocked() -> Bool {
        let sharedPreferences = SharedPreferences()
        return sharedPreferences.checkKey(ID_RINGTONES);
    }
    
    static func isAdsRemoved() -> Bool {
        let sharedPreferences = SharedPreferences()
        return sharedPreferences.checkKey(ID_NO_ADS);
    }
    
}
