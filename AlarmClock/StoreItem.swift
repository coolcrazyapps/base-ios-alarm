//
//  StoreItem.swift
//  AlarmClock
//
//  Created by Mohammad Shabaz Moosa on 07/07/15.
//  Copyright (c) 2015 Bazzinga Labs. All rights reserved.
//

class StoreItem {
    var title: String = ""
    var description: String = ""
    var imageName: String = ""
    var id: String = ""
    var isOwned: Bool = false
    
    init(id: String, title: String, description: String, imageName: String, isOwned: Bool) {
        self.id = id
        self.title = title
        self.description = description
        self.imageName = imageName
        self.isOwned = isOwned
    }
}
