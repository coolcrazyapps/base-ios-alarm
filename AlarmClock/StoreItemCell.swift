//
//  StoreItemCell.swift
//  AlarmClock
//
//  Created by Mohammad Shabaz Moosa on 07/07/15.
//  Copyright (c) 2015 Bazzinga Labs. All rights reserved.
//

import UIKit

class StoreItemCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var itemImage: UIImageView!
    
}