//
//  SwitchSettingTableViewCell.swift
//  AlarmClock
//
//  Created by Mohammad Shabaz Moosa on 28/06/15.
//  Copyright (c) 2015 Bazzinga Labs. All rights reserved.
//

import UIKit

protocol SwitchSettingTableViewCellDelegate: class {
    func switchSettingOnValueChanged(cell: SwitchSettingTableViewCell, sender: UISwitch)
}

class SwitchSettingTableViewCell: UITableViewCell {
    var id: String!
    var delegate: SwitchSettingTableViewCellDelegate!

    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var settingSwitch: UISwitch!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupCell(id: String, label: String, value: Bool, delegate: SwitchSettingTableViewCellDelegate){
        self.id = id
        descriptionLabel.text = label;
        settingSwitch.setOn(value, animated: false)
        self.delegate = delegate
        Themes.themes.initSwitchSettingTableViewCell(self as SwitchSettingTableViewCell)
    }

    @IBAction func settingSwitchOnValueChanged(sender: UISwitch) {
        delegate.switchSettingOnValueChanged(self, sender: sender)
    }
}
