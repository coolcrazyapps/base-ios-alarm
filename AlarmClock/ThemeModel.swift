//
//  ThemeModel.swift
//  AlarmClock
//
//  Created by Manpreet Singh Bedi on 22/07/15.
//  Copyright (c) 2015 Bazzinga Labs. All rights reserved.
//

import Foundation
import UIKit

class ThemeModel
{
    var background: String = "bg"
    var cellBackground: String = "bg"
    var textColor : UIColor = UIColor.whiteColor()
    var navigationBarColor : String = "bg"
    var navigationBarButtonColor : UIColor = UIColor.whiteColor()
    var navigationBarTitleTextColor : UIColor = UIColor.whiteColor()
    var tableViewCellColorEvilModeOn : UIColor = UIColor.redColor()
    var tableViewCellColorEvilModeOff : UIColor = UIColor.whiteColor()
    var globalTintColor : UIColor = UIColor.whiteColor()
    var switchThumbTintColor : UIColor = UIColor(hex: "#E74B60")
    var switchTintColor : UIColor = UIColor(hex: "#EFEFF4")
}