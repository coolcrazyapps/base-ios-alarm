//
//  Themes.swift
//  AlarmClock
//
//  Created by Manpreet Singh Bedi on 21/07/15.
//  Copyright (c) 2015 Bazzinga Labs. All rights reserved.
//

import Foundation
import UIKit

class Themes
{
    static let themes = Themes()
    let theme = ThemeModel()
    
    func initThemes(window: UIWindow?)
    {
        window!.tintColor = theme.globalTintColor
    }
    
    func initAlarmsListViewController(setViewController: AlarmsListViewController)
    {
        setViewController.navigationController?.navigationBar.barTintColor = UIColor(patternImage: UIImage(named: theme.navigationBarColor)!)
        setViewController.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : theme.navigationBarTitleTextColor]
        setViewController.view.backgroundColor = UIColor(patternImage: UIImage(named: theme.background)!)
        setViewController.addButton.tintColor = theme.navigationBarButtonColor
    }
    
    func initAlarmsDetailViewController(setViewController: AlarmDetailViewController)
    {
        setViewController.navigationController?.navigationBar.barTintColor = UIColor(patternImage: UIImage(named: theme.navigationBarColor)!)
        setViewController.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : theme.navigationBarTitleTextColor]
        setViewController.view.backgroundColor = UIColor(patternImage: UIImage(named: theme.background)!)
        setViewController.tableView.backgroundColor = UIColor(patternImage: UIImage(named: theme.background)!)
        setViewController.timePicker.backgroundColor = UIColor(patternImage: UIImage(named: theme.background)!)
        setViewController.timePicker.setValue(theme.textColor, forKey: "textColor")
        setViewController.cancelButton.tintColor = theme.navigationBarButtonColor
        setViewController.saveButton.tintColor = theme.navigationBarButtonColor
    }
    
    func initSettingsListViewController(setViewController : SettingsListViewController)
    {
        setViewController.navigationController?.navigationBar.barTintColor = UIColor(patternImage: UIImage(named: theme.navigationBarColor)!)
        setViewController.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : theme.navigationBarTitleTextColor]
        setViewController.tableView.backgroundColor = UIColor(patternImage: UIImage(named: theme.background)!)
    }
    
    func initShopListViewController(setViewController : ShopListViewController)
    {
        setViewController.navigationController?.navigationBar.barTintColor = UIColor(patternImage: UIImage(named: theme.navigationBarColor)!)
        setViewController.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : theme.navigationBarTitleTextColor]
        setViewController.tableView.backgroundColor = UIColor(patternImage: UIImage(named: theme.background)!)
    }
    
    func initSelectOptionTableViewController(setViewController: SelectOptionTableViewController)
    {
        setViewController.navigationController?.navigationBar.barTintColor = UIColor(patternImage: UIImage(named: theme.navigationBarColor)!)
        setViewController.navigationController?.navigationBar.tintColor = theme.textColor
            setViewController.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : theme.navigationBarTitleTextColor]
        setViewController.view.backgroundColor = UIColor(patternImage: UIImage(named: theme.background)!)
        setViewController.tableView.backgroundColor = UIColor(patternImage: UIImage(named: theme.background)!)
        setViewController.leftBarButton!.tintColor = theme.navigationBarButtonColor
    }
    
    func initEditLabelViewController(setViewController: EditLabelViewController)
    {
        setViewController.navigationController?.navigationBar.barTintColor = UIColor(patternImage: UIImage(named: theme.navigationBarColor)!)
        setViewController.navigationController?.navigationBar.tintColor = theme.textColor
        setViewController.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : theme.navigationBarTitleTextColor]
        setViewController.view.backgroundColor = UIColor(patternImage: UIImage(named: theme.background)!)
    }
    
    func initStoreItemCell(storeItemCell : StoreItemCell)
    {
        storeItemCell.backgroundColor = UIColor(patternImage: UIImage(named: theme.cellBackground)!)
        storeItemCell.titleLabel.textColor = theme.textColor
        storeItemCell.descriptionLabel.textColor = theme.textColor
    }
    func initAlarmTableViewCell(alarmTableViewCell : AlarmTableViewCell , alarmModel : AlarmModel)
    {
        alarmTableViewCell.backgroundColor = UIColor(patternImage: UIImage(named: theme.cellBackground)!)
        alarmTableViewCell.timeLabel.textColor = theme.textColor
        alarmTableViewCell.descriptionLabel.textColor = theme.textColor
        if(alarmModel.evilMode)
        {
            alarmTableViewCell.cellBorder.backgroundColor = theme.tableViewCellColorEvilModeOn
        }
        else
        {
            alarmTableViewCell.cellBorder.backgroundColor = theme.tableViewCellColorEvilModeOff
        }
        alarmTableViewCell.enabledSwitch.thumbTintColor = theme.switchThumbTintColor
        alarmTableViewCell.enabledSwitch.onTintColor = theme.switchTintColor
    }
    func initSwitchSettingTableViewCell(switchSettingTableViewCell : SwitchSettingTableViewCell)
    {
        switchSettingTableViewCell.backgroundColor = UIColor(patternImage: UIImage(named: theme.cellBackground)!)
        switchSettingTableViewCell.descriptionLabel.textColor = theme.textColor
        switchSettingTableViewCell.settingSwitch.thumbTintColor = theme.switchThumbTintColor
        switchSettingTableViewCell.settingSwitch.onTintColor = theme.switchTintColor
    }
    func initLabelSettingTableViewCell(labelSettingTableViewCell : LabelSettingTableViewCell)
    {
        labelSettingTableViewCell.backgroundColor = UIColor(patternImage: UIImage(named: theme.cellBackground)!)
        labelSettingTableViewCell.descriptionLabel.textColor = theme.textColor
        labelSettingTableViewCell.valueLabel.textColor = theme.textColor
    }
    func initDeleteAlarmTableViewCell(deleteAlarmTableViewCell : DeleteAlarmTableViewCell)
    {
        deleteAlarmTableViewCell.backgroundColor = UIColor(patternImage: UIImage(named: theme.cellBackground)!)
        deleteAlarmTableViewCell.descriptionLabel.textColor = theme.textColor
    }
    func initOptionsAlarmTableViewCell(optionsAlarmTableViewCell : OptionsAlarmTableViewCell)
    {
        optionsAlarmTableViewCell.backgroundColor = UIColor(patternImage: UIImage(named: theme.cellBackground)!)
        optionsAlarmTableViewCell.descriptionLabel.textColor = theme.textColor
    }
    func initCustomHeaderTableViewCell(customHeaderTableViewCell : CustomHeaderTableViewCell)
    {
        customHeaderTableViewCell.descriptionLabel.textColor = theme.textColor
    }
    func initOptionsItemTableViewCell(uiTableViewCell: UITableViewCell)
    {
        uiTableViewCell.backgroundColor = UIColor(patternImage: UIImage(named: theme.cellBackground)!)
        uiTableViewCell.textLabel!.textColor = theme.textColor
        uiTableViewCell.tintColor = theme.textColor
    }
}
