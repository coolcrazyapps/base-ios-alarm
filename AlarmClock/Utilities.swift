//
//  Utilities.swift
//  AlarmClock
//
//  Created by Mohammad Shabaz Moosa on 30/06/15.
//  Copyright (c) 2015 Bazzinga Labs. All rights reserved.
//
import UIKit

extension String {
    
    func contains(find: String) -> Bool{
        return self.rangeOfString(find) != nil
    }
}

class Utilities {
    static func getSnoozeOptions() -> [Option] {
        var options: [Option] = []
        options.append(Option(id: String(-1), name: "Unlimited snoozes", value: -1, type: "number"))
        for(var i = 1; i <= 5; i++){
            options.append(Option(id: String(i), name: "\(i) snoozes", value: i, type: "number"))
        }
        return options
    }
    
    static func getStepCountOptions() -> [Option] {
        var options: [Option] = []
        for(var i = 5; i <= 100; i+=5){
            options.append(Option(id: String(i), name: "\(i) steps", value: i, type: "number"))
        }
        return options
    }
    
    static func getStepSensitivityOptions() -> [Option] {
        var options: [Option] = [
            Option(id: "low", name: "Low", value: 0.2, type: "float"),
            Option(id: "medium", name: "Medium", value: 0.25, type: "float"),
            Option(id: "high", name: "High", value: 0.3, type: "float")
        ]
        return options
    }

    static func getShakeSensitivityOptions() -> [Option] {
        var options: [Option] = [
            Option(id: "low", name: "Low", value: 0.5, type: "float"),
            Option(id: "medium", name: "Medium", value: 0.6, type: "float"),
            Option(id: "high", name: "High", value: 0.7, type: "float")
        ]
        return options
    }

    static func getSelectedOption(value: AnyObject, options: [Option]) -> Option {
        var type = options[0].type
        for(var i = 0; i < options.count; i++){
            var option = options[i];
            switch type {
            case "number":
                if let searchValue = value as? Int {
                    if(option.value as! Int == searchValue){
                        return option
                    }
                }
            case "string":
                if let searchValue = value as? String {
                    if(option.value as! String == searchValue){
                        return option
                    }
                }
            case "float":
                if let searchValue = value as? Float {
                    if(option.value as! Float == searchValue){
                        return option
                    }
                }
            default:
                println("getSelectedOption: Unknown type encountered")
            }        }
        return options[0];
    }
    
    static func getTimeFromNow(time: NSDate) -> String {
        var interval = Int(time.timeIntervalSinceNow)
        let hours = interval/3600
        let minutes = interval/60 % 60
        
        var description = ""
        if hours != 0 {
            description = "\(hours) hours"
        }
        if minutes != 0 {
            description = "\(description) \(minutes) minutes"
        }
        if description == "" {
            description = "a few moments"
        }
        return description
    }
    
    static func getAlarmToneOptions() -> [Option] {
        var alarmTones = getAlarmTonesList()
        var options: [Option] = []
        for (key, value) in alarmTones
        {
            options.append(Option(id: value, name: key, value: value, type: "string"))
        }
        return options
    }
    static func getSnoozeDurationOptions() -> [Option] {
        var options: [Option] = []
        for(var i = 2; i <= 5; i++){
            options.append(Option(id: String(i), name: "\(i) minutes", value: i, type: "number"))
        }
        options.append(Option(id: "10", name: "10 minutes", value: 10, type: "number"))
        
        return options
    }
    static func getAlarmTonesList() -> Dictionary<String, String>
    {
        var alarmTones = Dictionary<String , String>()
        alarmTones.updateValue("Animals.mp3", forKey: "Animals")
        alarmTones.updateValue("Alarm.mp3", forKey: "Basic Alarm")
        alarmTones.updateValue("Beep.mp3", forKey: "Beep")
        alarmTones.updateValue("Cellphone.mp3", forKey: "Cellphone")
        alarmTones.updateValue("Chant.mp3", forKey: "Chant")
        alarmTones.updateValue("Factory_Alarm.mp3", forKey: "Factory Alarm")
        alarmTones.updateValue("Jaws Theme.mp3", forKey: "Jaws Theme")
        alarmTones.updateValue("School Bell.mp3", forKey: "School Bell")
        alarmTones.updateValue("Premium_Bomb.mp3", forKey: "Bomb (Premium)")
        alarmTones.updateValue("Premium_Chicken Mix.mp3", forKey: "Chicken Mix (Premium)")
        alarmTones.updateValue("Premium_Dreamy.mp3", forKey: "Dreamy (Premium)")
        alarmTones.updateValue("Premium_Evil Laugh.mp3", forKey: "Evil Laugh (Premium)")
        alarmTones.updateValue("Premium_Musical.mp3", forKey: "Musical (Premium)")
        alarmTones.updateValue("Premium_New Day.mp3", forKey: "New Day (Premium)")
        alarmTones.updateValue("Premium_Wake Up.mp3", forKey: "Wake Up (Premium)")
        return alarmTones
    }
    
    static func valueToDisplayForOption(value : AnyObject , options:[Option]) -> String
    {
        var type = options[0].type
        for option in options
        {
            if type == "number" {
                if let searchValue = value as? Int {
                    if(option.value as! Int == searchValue)
                    {
                        return option.name
                    }
                }
            }
            else if type == "string" {
                if let searchValue = value as? String {
                    if(option.value as! String == searchValue)
                    {
                        return option.name
                    }
                }
            }
            else if type == "float" {
                if let searchValue = value as? Float {
                    if(option.value as! Float == searchValue)
                    {
                        return option.name
                    }
                }
            }
        }
        return "Unknown"
    }
    
    static func isPremiumTone(option: Option) -> Bool {
        return option.name.contains("Premium")
    }
    
    static func getWeekDay(today:String) -> String
    {
            let formatter  = NSDateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            let todayDate = formatter.dateFromString(today)!
            let myCalendar = NSCalendar(calendarIdentifier: NSCalendarIdentifierGregorian)!
            let myComponents = myCalendar.components(.CalendarUnitWeekday, fromDate: todayDate)
            let weekDay = myComponents.weekday
            return weekDayNumberToString(weekDay)
    }
    static func weekDayNumberToString(weekday: Int) -> String
    {
        switch weekday
        {
        case 1 :
            return "SUNDAY"
        case 2 :
            return "MONDAY"
        case 3 :
            return "TUESDAY"
        case 4 :
            return "WEDNESDAY"
        case 5 :
            return "THURSDAY"
        case 6 :
            return "FRIDAY"
        case 7 :
            return "SATURDAY"
        default :
            return "MONDAY"
        }
    }
}
